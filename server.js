const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const {getServerIP} = require('./app/utils/common')

const app = express();

var corsOptions = {
  origin: "http://localhost:3040"
};

//app.use(cors(corsOptions));
app.use(cors())

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
//body-parser helps to parse the request and create the req.body object


//database connection
const db = require('./app/models')
//Model synchronisation.. sychronise all models with database..
db.sequelize.sync()
// // drop the table if it already exists
// good for development.. but destructive for production..
// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });


// test multi-store retail server health.
app.get("/pingServer", (req, res) => {
  
   db.sequelize.authenticate()
      .then(result=>{
            res.json({ 
              success: true,
              message: "Welcome to MultiStore Retail Server" 
            });
      })
      .catch(err=>{
        console.log('Database server is down. You need to turn it on')
        res.json({
          success: false,
          message: 'Database service is down. You need to turn it on'
        })
      })
  
});

//include the mounted routes here .. pay attention to the authentication middleware
require('./app/routes/product.routes')(app)
require('./app/routes/stocklist.routes')(app)
require('./app/routes/sales.routes')(app)
require('./app/routes/posDetail.routes')(app)
require('./app/routes/saleOrder.routes')(app)
require('./app/routes/purchaseOrder.routes')(app)
require('./app/routes/contact.routes')(app)
require('./app/routes/expense.routes')(app)
require('./app/routes/creditPayment.routes')(app)
require('./app/routes/business.routes')(app)

//what about the IP address of the server.
console.log("===> RETAIL SERVER IP ADDRESS ==>", getServerIP())

// set port, listen for requests
const PORT = process.env.PORT || 3040;
app.listen(PORT, () => {
  console.log(`MULTI-STORE Server is running on port ${PORT}.`);
});