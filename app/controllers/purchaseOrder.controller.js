/*
* PURCHASE-ORDER CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const purchaseOrder = db.purchaseOrder
const Product = db.products
const Op = db.Sequelize.Op

const { updateProductAfterPurchaseOrder } = require('./product.controller')
const { createNewCreditPayment, getTotalCreditPayments } = require('./creditPayment.controller')

const { calculateTotalPurchases } = require('../utils/common')



exports.createPurchaseOrder  = (req,res)=>{

    const errors = validationResult(req)
    
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    const products = req.body.products
    const newPurchaseOrder = {
       totalAmount: parseFloat(req.body.totalAmount),
       paymentOption: req.body.paymentOption,
       client: req.body.client,
       products: JSON.stringify(products),//allow string not array
       depositAmount: parseFloat(req.body.depositAmount),
       order_date: new Date(req.body.order_date),
       receivedDate: new Date(req.body.receivedDate),
       status: req.body.status,
       actor: req.body.actor,
       shopId: req.body.shopId,
       synced: true, 
       closed:false //for deletion purposes     
    }
    

    //Save the purchaseOrder in the database.
    purchaseOrder.create(newPurchaseOrder)
        .then(data=>{

            updateProductAfterPurchaseOrder(products)
               .then(result=>{
                    console.log('updated product after purchaseOrder', result)
                    if (newPurchaseOrder.paymentOption === 'credit') {
                        this.creditPaymentForPurchaseOrder(data)
                            .then(result=>{
                                console.log('successful recorded credit payments', result)
                            })
                            .catch(err=>{
                               console.log('Failed to record creditPayment', err)
                            })
                    }
                    res.status(201).send({
                        success:true,
                        message:data
                    })
                         
               })
               .catch(err=>{
                    res.status(422).send({
                      success: false,
                      message:
                        err.message || "Some error occurred while creating the purchaseOrder"
                    });
               })
            //add support for creditPayment deposit here

        })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message:
                  err.message || "Some error occurred while creating the purchaseOrder."
            });
        })
}

//Retrieve all purchaseOrder for that particular shopId from the database.
exports.findAllPurchaseOrders = (req, res) => {

    const shopId = req.query.shopId //to get query string from the Request
    var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed: false} : null,
        orderFormat = [
            ['order_date', 'DESC']
        ]
    purchaseOrder.findAll({ where: condition, order:orderFormat })
         .then(data => {
            res.status(200).send({
               success: true, 
               orderList: data
            });
         })
         .catch(err => {
              res.status(500).send({
                 success: false,
                 message:
                   err.message || "Some error occurred while retrieving purchaseOrder"
              });
         });
};


  // Update a purchaseOrderItem by the id in the request
exports.updatePurchaseOrderItem = (req, res) => {
    
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const id = req.params.id,
          updateItemBody = req.body.updatedFields,
          updateObject = {
            products: JSON.stringify(updateItemBody.products),
            totalAmount: parseFloat(updateItemBody.totalAmount),
            status: updateItemBody.status
          },
          productUpdates = req.body.productUpdates,
          removeProducts = req.body.removeProducts

    purchaseOrder.update(updateObject,{
       where:{referenceId:id}   
    })
    .then(num => {
        if (num == 1) {
          //purpose of updating the productStock using productInfo 
            if (productUpdates.length > 0) {
                updateProductAfterPurchaseOrder(productUpdates, updateObject.status )
                    .then(result=>{
                        console.log('updated productItems for existing items in purchaseOrder', result)
                    })
                    .catch(err=>{
                        console.log('Failed to update the productItems for existing items in purchaseOrder', err)
                    })
            }

            if (removeProducts.length > 0) {
                let deleteProducts = removeProducts
                deleteProducts.forEach(item=>{
                    item.productQty = -parseFloat(item.productQty) //so that we can reuse below method
                })

                updateProductAfterPurchaseOrder(deleteProducts, updateObject.status)
                    .then(result=>{
                        console.log('updated the productItems after they have been removed in purchaseOrder', result) 
                    })
                    .catch(err=>{
                        console.log('failed to update productItems after they have been removed in purchaseOrder', err)
                    })
            }

            //u need to query for purchaseOrderItem by Id n push it in mobx
             setTimeout(() => {
                this.findPurchaseOrderById(id)
                    .then(orderObj=>{
                        res.status(200).send({
                            success:true,
                            message: orderObj
                        })
                    })
                    .catch(err=>{
                        res.status(412).send({
                            success: false,
                            message: 'Failed to retrieve the updated purchaseOrderItem'
                        })
                    })
             }, 2000);

        } else {
          res.status(200).send({
            success: false,
            message: `Cannot update purchaseOrderItem with id=${id}. Maybe purchaseOrderItem was not found or req.body is empty!`
          });
        }
       })
       .catch(err => {
          res.status(500).send({
            success: false,
            message: "Error updating purchaseOrder with id=" + id
        });
    });

};


//Retrieve a single purchaseOrder with products info.
exports.findPurchaseOrderById = (saleId)=>{
    return new Promise((resolve,reject)=>{
        purchaseOrder.findByPk(saleId)
            .then(result=>{
                resolve(result)
            })
            .catch(err=>{
                reject(err)
            })
    })
}

exports.deletePurchaseOrderItem = (req,res)=>{
    //just close the saleItem.
    const purchaseId = req.params.id,
          updateObj = {
            closed:true
          }
    debugger;
    purchaseOrder.update(updateObj, {
            where: {referenceId: purchaseId}
        })
        .then(num=>{
            if (num == 1) {
              res.send({
                 success: true,
                 message: "PurchaseOrderItem was deleted successfully!"
              });
            } else {
              res.send({
                success: false,
                message: `Cannot delete PurchaseOrderItem with id=${id}. Maybe PurchaseOrderItem was not found!`
              });
            }    
        })
        .catch(err=>{
            res.status(500).send({
               success: false,
               message: "Could not delete PurchaseOrderItem with id=" + id
            });
    })

}


exports.pickTotalPurchaseOrder = (req,res)=>{
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
       return res.status(422).json({ errors: errors.array() })
    }

    const startDate = new Date(req.body.startDate),
            endDate = new Date(req.body.endDate)
    const shopId = req.body.shopId //to get query string from the Request
    var condition =  shopId ? 
            {
                shopId: {[Op.like]:`%${shopId}%`}, 
                closed: false,
                order_date: {[Op.between]: [startDate, endDate]}
            }
                : 
            null;
    
    purchaseOrder.findAll({where: condition})
        .then(filteredPurchaseOrders =>{
                const totalOrders = calculateTotalPurchases(filteredPurchaseOrders)
                res.status(200).send({
                    success: true,
                    message: totalOrders
                })
        })
        .catch(err=>{
              res.status(500).send({
                success: false,
                message:
                    err.message || "Some error occurred while retrieving purchaseOrders"
              });
        })
      
}


///sale order metrics for credit purposes.
exports.getTotalCreditPurchases = (input)=>{
    return new Promise((resolve,reject)=>{
        const startDate = new Date(input.startDate),
              endDate = new Date(input.endDate),
              shopId = req.body.shopId //to get query string from the Request
        var condition =  shopId ? 
                        {
                          shopId: {[Op.like]:`%${shopId}%`}, 
                          closed: false,
                          paymentOption: 'credit',
                          order_date: {[Op.between]: [startDate, endDate]}
                        }
                          : 
                        null;
        purchaseOrder.findAll({where:condition})
                .then(filteredCreditPurchases =>{
                    const totalCreditPurchases = calculateTotalPurchases(filteredCreditPurchases)
                    resolve(totalCreditPurchases)
                })
                .catch(err=>{
                    reject(err)
                })
      
       })
  }

//calculate the creditPayments debt for sales.
exports.purchaseCreditDebt = (input)=>{
    return new Promise((resolve,reject)=>{
        this.getTotalCreditPurchases(input)
            .then(result=>{
                getTotalCreditPayments(input, 'sale')
                    .then(totalDeposit=>{
                      const metrics = {
                         saleCredit:result,
                         amountPaid: totalDeposit
                      }
                      res.status(200).send({
                        success: true,
                        message: metrics
                      })
                })
            })
            .catch(err=>{
                res.status(500).send({
                   success: false,
                   message: err.message | "Error in generating the total credit sales metrics"
                })
            })
    }) 
}

exports.creditPaymentForPurchaseOrder = (newOrder)=>{
    return new Promise((resolve,reject)=>{
        const depositAmount = parseFloat(newOrder.depositAmount),
              originalDebt =  parseFloat(newOrder.totalAmount)

        const newDeposit = {
           referenceId: newOrder.referenceId,
           category: 'purchase-order',
           contactId: newOrder.client,
           depositAmount: depositAmount,
           originalDebt: originalDebt,
           balance: originalDebt - depositAmount,
           note: "The purchase-order was extended via credit",
           soldAt: new Date(newOrder.order_date) || new Date(),
           closed: false,
           shopId: newOrder.shopId,
           synced: true
       }

        createNewCreditPayment(newDeposit)
            .then(result=>{
                resolve(result)
            })
            .catch(err=>{
              reject(err)
            })

    })
}

/**
 * SKIPPED THESE METHODS.
 * - purchaseOrderCreditDebt (incomplete . WIP)
 * - handleImportPurchaseOrder
 */
  