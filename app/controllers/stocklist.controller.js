/*
* STOCKLIST CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const Stocklist = db.stocklist
const Op = db.Sequelize.Op

const {updateProductAfterStocklist} = require('./product.controller')
const { createNewCreditPayment } = require('./creditPayment.controller')


//Create and save the new Stocklist.
exports.createNewStock  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
   //Create the product
    const stocklist = {
       //productId: req.body.productId,
       productId: req.body.productId,
       costPrice: req.body.costPrice,
       sales_qty: req.body.sales_qty,
       who_sold: req.body.who_sold,
       arrivalDate: new Date(req.body.arrivalDate) || new Date(),
       batchCode: req.body.batchCode,
       paymentOption: req.body.paymentOption,
       depositAmount: req.body.depositAmount,
       supplierId: req.body.supplierId,
       shopId: req.body.shopId,
       synced: true, //when not connected to the internet
       closed:false //for deletion purposes     
    }

    //Save the product in the database.
    Stocklist.create(stocklist)
        .then(data=>{
            updateProductAfterStocklist(data)
               .then(result=>{
                    console.log('updated result', result)
                    
                    //add support for the creditPayments
                    if (stocklist.paymentOption === 'credit') {
                        this.creditPaymentForStocklist(data)
                           .then(result=>{
                              console.log('sucessful recorded credit deposit', result)
                           })
                           .catch(err=>{
                              console.log('failed to record credit payment', err)
                           })
                    }
                    
                    this.findStocklistById(data.id)
                        .then(stock=>{
                          res.status(201).send({
                            success:true,
                            message: stock
                          })
                        })
                        .catch(err=>{
                            res.status(412).send({
                              success: false,
                              message: 'Failed to retrieve the created newStock'
                            })
                        })
                         
               })
               .catch(err=>{
                    res.status(422).send({
                      success: false,
                      message:
                        err.message || "Some error occurred while creating the stocklist."
                    });
               })
            
        })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message:
                  err.message || "Some error occurred while creating the stocklist."
            });
        })
}


// Retrieve all Stocklist for that particular shopId from the database.
exports.findAllStocklist = (req, res) => {
    //Retrieve all Stocklist/ find by shopId from the database:
    const shopId = req.query.shopId //to get query string from the Request
    //var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}} : null
    var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed: false} : null,
        orderFormat = [
          ['arrivalDate', 'DESC']
        ]
    Stocklist.findAll({ where: condition, order:orderFormat, include: ['product'] })
         .then(data => {
           //use the INNER JOIN with products db before sending results
           res.status(200).send({
             success: true, 
             stocklist: data
           });
         })
         .catch(err => {
           res.status(500).send({
              message:
               err.message || "Some error occurred while retrieving Incoming Stock."
           });
         });
 };

 //Retrieve a single stocklist with products info.
exports.findStocklistById = (stocklistId)=>{
    return new Promise((resolve,reject)=>{
      Stocklist.findByPk(stocklistId, {include: ['product']})
          .then(result=>{
            resolve(result)
          })
          .catch(err=>{
            reject(err)
          })
      })
}

// Update a Stocklist by the id in the request
exports.updateStocklistItem = (req, res) => {
    
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
  }
  
  const id = req.params.id,
        updateItemBody = req.body.updateItem,
        updateObject = {
          sales_qty: updateItemBody.sales_qty,
          arrivalDate: new Date(updateItemBody.arrivalDate),
          batchCode: updateItemBody.batchCode,
          costPrice: updateItemBody.costPrice
        };

  Stocklist.update(updateObject,{
     where:{id:id}   
  })
  .then(num => {
      if (num == 1) {
        //purpose of updating the productStock using productInfo
        const newStock = {
           productId: req.body.productId,
           sales_qty: req.body.productStock
        }
        updateProductAfterStocklist(newStock)
           .then(result=>{

                this.findStocklistById(id)
                      .then(stock=>{
                        res.status(200).send({
                            success:true,
                            message: stock
                        })
                      })
                      .catch(err=>{
                          res.status(412).send({
                            success: false,
                            message: 'Failed to retrieve the updated Stocklist'
                          })
                      })
           })
           .catch(err=>{

           })
      } else {
        res.status(200).send({
          success: false,
          message: `Cannot update Stocklist with id=${id}. Maybe Stocklist was not found or req.body is empty!`
        });
      }
     })
     .catch(err => {
        res.status(500).send({
          success: false,
          message: "Error updating Stocklist with id=" + id
        });
      });
  };

  exports.deleteStocklistItem = (req,res)=>{
      //just close the product.
      const stockId = req.params.id,
            updateObj = {
              closed:true
            }
       Stocklist.update(updateObj, {
              where: {id: stockId}
          })
          .then(num=>{
              if (num == 1) {
                res.send({
                   success: true,
                   message: "Stocklist was deleted successfully!"
                });
              } else {
                res.send({
                  success: false,
                  message: `Cannot delete Stocklist with id=${id}. Maybe Stocklist was not found!`
                });
              }    
          })
          .catch(err=>{
              res.status(500).send({
                 success: false,
                 message: "Could not delete Stocklist with id=" + id
              });
          })

  }


  exports.calculateTotalPurchases = (req,res)=>{
        const errors = validationResult(req)
        if (! errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() })
        }

        const startDate = new Date(req.body.startDate),
              endDate = new Date(req.body.endDate)
              const shopId = req.body.shopId //to get query string from the Request
              var condition =  shopId ? 
                              {
                                shopId: {[Op.like]:`%${shopId}%`}, 
                                closed: false,
                                arrivalDate: {[Op.between]: [startDate, endDate]}
                              }
                              : 
                              null;

              Stocklist.findAll({ where: condition })
                  .then(filteredStock => {
                       let no_of_purchases = filteredStock.length,
                           totalPurchaseAmount = 0;

                        filteredStock.forEach((stock)=>{
                           let purchaseCost = parseFloat(stock.costPrice || 0) * stock.sales_qty;
                           totalPurchaseAmount+=purchaseCost 
                        })

                        const result = {
                           purchaseItems:no_of_purchases,
                           purchaseAmount:totalPurchaseAmount
                        }
                      
                        res.status(200).send({
                           success: true, 
                           message: result
                        });
                  })
                  .catch(err => {
                      res.status(500).send({
                         success: false,
                         message:
                            err.message || "Some error occurred while retrieving Incoming Stock."
                      });
              });
  }

  exports.creditPaymentForStocklist = (newStock)=>{

      return new Promise((resolve,reject)=>{

          const originalDebt = parseFloat(newStock.costPrice * newStock.sales_qty),
                depositAmount = parseFloat(newStock.depositAmount)

          const newDeposit = {
              referenceId: newStock.referenceId, //generated from new stock
              category: 'new-stock',
              contactId: newStock.supplierId,
              depositAmount: depositAmount,
              originalDebt: originalDebt,
              balance: originalDebt - depositAmount,
              note: "The new-stock was extended via credit",
              soldAt: new Date(newStock.arrivalDate) || new Date(),
              closed: false,
              shopId: newStock.shopId,
              synced: true
          }

          createNewCreditPayment(newDeposit)
              .then(result=>{
                   resolve(result)
              })
              .catch(err=>{
                  reject(err)
              })
          })
   }


  /*** Skipping this methods for now */
  /*
  - handleImportStock(stocklist) --> for bulk import
  */


