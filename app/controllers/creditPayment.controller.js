/*
* CREDIT PAYMENTS CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const CreditPayment = db.CreditPayment
const Op = db.Sequelize.Op

const { calculateTotalDeposit } = require('../utils/common')



//Create and save the new Sale.
exports.createNewCreditPayment  = (newDeposit)=>{
     
    return new Promise((resolve,reject)=>{
        //Save the creditDeposit in the database.
        CreditPayment.create(newDeposit)
            .then(data=>{
               resolve(data)
            })
            .catch(err=>{
                reject(err)
           })
    })
}


//Retrieve all CreditPayments / find by shopId from the database:
exports.findAllCreditPayment = (req, res) => {
    const shopId = req.query.shopId //to get query string from the Request
    var condition =  shopId ? { shopId: {[Op.like]:`%${shopId}%`}, closed:false} : null,
        orderFormat = [
            ['soldAt', 'DESC']
        ]
    
    return new Promise((resolve,reject)=>{
        CreditPayment.findAll({ where: condition, raw:true })
            .then(data => {
                res.status(200).send({
                   success: true, 
                   creditList: data
                });
            })
            .catch(err => {
                res.status(500).send({
                   message:
                      err.message || "Some error occurred while retrieving contacts"
                });
            });
    })

};


// Update a Contact by the id in the request
exports.updateCreditPayment = (req, res) => {
    
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const referenceId = req.params.id,
          updateItemBody = req.body.updatedFields,
          updateObject = {
            category: updateItemBody.category,
            note: updateItemBody.note,
            depositAmount: updateItemBody.depositAmount,
            balance: updateItemBody.balance
          };
    
    CreditPayment.update(updateObject,{
        where:{referenceId: referenceId}   
    })
    .then(num => {
        if (num == 1) {
          res.send({
            success: true,
            message: "CreditPayment was updated successfully."
          });
        } else {
          res.send({
            success: false,
            message: `Cannot update CreditPayment with id=${id}. Maybe CreditPayment was not found or req.body is empty!`
          });
        }
       })
       .catch(err => {
          res.status(500).send({
            success: false,
            message: "Error updating Contact with id=" + id
          });
        });
}; 


exports.getTotalCreditPayments = (inputDate,creditCategory)=>{
    return new Promise((resolve,reject)=>{
        const startDate = new Date(inputDate.startDate),
              endDate = new Date(inputDate.endDate)
              
              const shopId = inputDate.shopId //to get query string from the Request
              var condition =  shopId ? 
                      {
                          shopId: {[Op.like]:`%${shopId}%`}, 
                          closed: false,
                          category: creditCategory,
                          soldAt: {[Op.between]: [startDate, endDate]}
                      }
                          : 
                      null;
                      
                CreditPayment.findAll({where: condition})
                      .then(filteredCredits =>{

                          const totalDeposit = calculateTotalDeposit(filteredCredits)
                          resolve(totalDeposit)
                          
                      })
                  .catch(err=>{
                      reject(err)
                  })
        })
}

exports.multi_store_addCreditPayment = (req,res)=>{
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    const newDeposit = {
        referenceId: req.body.referenceId,
        category: req.body.category,
        contactId: req.body.contactId,
        depositAmount: parseFloat(req.body.depositAmount),
        originalDebt: parseFloat(req.body.originalDebt),
        balance: parseFloat(req.body.balance),
        note: req.body.note,
        soldAt: new Date(req.body.soldAt),
        shopId: req.body.shopId,
        closed: false,
        synced: true
    }

    this.createNewCreditPayment(newDeposit)
        .then(result=>{
            res.status(201).send({
                success: true,
                message: result
            });
        })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message: "Failed to add the credit payment to the retail server "
            });
        })
}