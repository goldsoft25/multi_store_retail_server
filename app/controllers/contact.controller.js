/*
* CONTACT CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const Contact = db.Contact
const Op = db.Sequelize.Op

const { defaultContacts } = require('../utils/sampleData')


//Create and save the new Sale.
exports.createNewContact  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const newContact = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        primary_contact: req.body.primary_contact,
        other_contact: req.body.other_contact,
        company_name: req.body.company_name,
        company_street: req.body.company_street,
        email: req.body.email,
        contact_type: req.body.contact_type,
        notes: req.body.notes,
        business_id: req.body.business_id,
        synced: true, 
        closed:false //for deletion purposes  
    }

    //Save the product in the database.
    Contact.create(newContact)
        .then(data=>{
           res.status(201).send({
              success:true,
              message: data
           })
        })
        .catch(err=>{
            res.status(500).send({
                message:
                  err.message || "Some error occurred while creating the Contact."
            });
        })
}

// Update a Contact by the id in the request
exports.updateContact = (req, res) => {
    
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const id = req.params.id,
          updateItemBody = req.body.updatedFields,
          updateObject = {
            primary_contact: updateItemBody.primary_contact,
            other_contact: updateItemBody.other_contact,
            company_name: updateItemBody.company_name,
            company_street: updateItemBody.company_street,
            email: updateItemBody.email,
            notes: updateItemBody.notes
          };
    
    Contact.update(updateObject,{
        where:{contactId:id}   
    })
    .then(num => {
        if (num == 1) {
          res.send({
            success: true,
            message: "Contact was updated successfully."
          });
        } else {
          res.send({
            success: false,
            message: `Cannot update Contact with id=${id}. Maybe Contact was not found or req.body is empty!`
          });
        }
       })
       .catch(err => {
          res.status(500).send({
            success: false,
            message: "Error updating Contact with id=" + id
          });
        });
};  

//Retrieve all Contacts/ find by shopId from the database:
exports.findAllContact = (req, res) => {
    const shopId = req.query.shopId //to get query string from the Request
    var condition =  shopId ? { business_id: {[Op.like]:`%${shopId}%`}, closed:false} : null,
        orderFormat = [
          ['createdAt', 'DESC']
        ]
  Contact.findAll({ where: condition, order:orderFormat, raw:true })
      .then(data => {
        let mergedContact = [...defaultContacts, ...data]
        res.status(200).send({
          success: true, 
          contacts: mergedContact
        });
      })
      .catch(err => {
        res.status(500).send({
           message:
            err.message || "Some error occurred while retrieving contacts"
        });
      });
};

exports.deleteContactItem = (req,res)=>{
  //just close the contactItem.
  const contactId = req.params.id,
        updateObj = {
          closed:true
        }
  
   Contact.update(updateObj, {
          where: {contactId: contactId}
      })
      .then(num=>{
          if (num == 1) {
              res.send({
                success: true,
                message: "ContactItem was deleted successfully!"
              });
          } else {
            res.send({
               success: false,
               message: `Cannot delete ContactItem with id=${id}. Maybe ContactItem was not found!`
            });
          }    
      })
      .catch(err=>{
          res.status(500).send({
             success: false,
             message: "Could not delete ContactItem with id=" + id
          });
      })

}

