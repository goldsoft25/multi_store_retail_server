/*
* POSDETAIL CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const POSDetail = db.posDetail
const Product = db.products
const Op = db.Sequelize.Op

const { updateProductAfterPOSDetail } = require('./product.controller')
const {getTotalPOSDetailSales , calculateTopSellerPOSDetails } = require('../utils/common')

//Create and save the new POSDetail.
exports.createNewPOSDetail  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    const receiptItems = req.body.receiptItems
    const newPOSDetail = {
       receiptNo: req.body.receiptNo,
       cashReceived: parseFloat(req.body.cashReceived),
       totalAmount: parseFloat(req.body.totalAmount),
       customerId: req.body.customerId,
       receiptItems: JSON.stringify(receiptItems),//allow string not array
       actor: req.body.actor,
       shopId: req.body.shopId,
       synced: true, //when not connected to the internet
       closed:false //for deletion purposes     
    }

    //Save the posDetail in the database.
    POSDetail.create(newPOSDetail)
        .then(data=>{
            
            updateProductAfterPOSDetail(receiptItems)
               .then(result=>{
                    console.log('updated product after POS', result)
                          res.status(201).send({
                            success:true,
                            message:data
                          })
                         
               })
               .catch(err=>{
                    res.status(422).send({
                      success: false,
                      message:
                        err.message || "Some error occurred while creating the POSDetail"
                    });
               })
            //add support for creditPayment deposit here
        })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message:
                  err.message || "Some error occurred while creating the Sales."
            });
        })
}

//Retrieve all POSDetail for that particular shopId from the database.
exports.findAllPOSDetail = (req, res) => {

  const shopId = req.query.shopId //to get query string from the Request
  var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed: false} : null,
      orderFormat = [
          ['createdAt', 'DESC']
      ]

  POSDetail.findAll({ where: condition, order: orderFormat})
       .then(data => {
          res.status(200).send({
             success: true, 
             posList: data
          });
       })
       .catch(err => {
            res.status(500).send({
               success: false,
               message:
                 err.message || "Some error occurred while retrieving POSDetail"
            });
       });
};

////dealing with metrics for pos items.
exports.filteredPOSItemsByDate = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    const startDate = new Date(req.body.startDate), 
         endDate = new Date(req.body.endDate),
         shopId = req.body.shopId;

    const condition =  shopId ? 
                          {
                              shopId: {[Op.like]:`%${shopId}%`}, 
                              closed: false,
                              createdAt: {[Op.between]: [startDate, endDate]}
                          }
                          : 
                        null;
    
    POSDetail.findAll({where:condition, raw: true})
        .then(filteredPOS =>{
              const posDetailSales = getTotalPOSDetailSales(filteredPOS)

              this.groupPOSItemsByProdName(filteredPOS)
                  .then(groupedPOS=>{
                    const result = {
                       posDetailSales: posDetailSales,
                       topPOSItems: groupedPOS
                    }

                    res.status(200).send({
                       success: true,
                       message:result
                   })
              })
              .catch(err=>{
                    res.status(422).send({
                       success: false,
                       message:  err.message | "Failed to generate the POSDetail metrics"
                    })
              })

        })
        .catch(err=>{
              res.status(500).send({
                success: false,
                message:
                   err.message || "Some error occurred while retrieving POSDetail"
              });
        })
};

exports.groupPOSItemsByProdName = (posItemSales)=>{
  let groupedPOSSales = []

  return new Promise((resolve,reject)=>{
     
      Product.findAll({where: {closed: false}})
            .then(result=>{
              const products = result
              products.forEach(item=>{
                  const topPOSItems = calculateTopSellerPOSDetails(posItemSales, item.prod_name)

                  if(topPOSItems.totalSaleQty >= 1){ //get rid of 0 sales
                    groupedPOSSales.push({
                       productName: topPOSItems.productName,
                       //totalSales: topPOSItems.totalSaleQty 
                        salesQty:topPOSItems.totalSaleQty
                    })
                  }
              })
                //sort them for topSellingItems.
                groupedPOSSales.sort((a,b)=>b.salesQty - a.salesQty)
                resolve(groupedPOSSales)
            })
            .catch(err=>{
               reject(err)
            })
      })

}


/***
 *  Skipped these posItems.
 * - handleImportPOSDetails(posItems)
 */
