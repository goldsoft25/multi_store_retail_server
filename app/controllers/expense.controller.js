/*
* EXPENSE CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const Expense = db.Expense
const ExpenseCategory = db.ExpenseCategory
const Op = db.Sequelize.Op
const { isValidDate, getTotalExpenses } = require('../utils/common')
const { getDefaultExpenseCategory } = require('../utils/sampleData')



exports.createNewExpense  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const approvedDate = req.body.approved_on
    const newExpense = {
        exp_category:req.body.exp_category,
        expense_description: req.body.expense_description,
        exp_amount: parseFloat(req.body.exp_amount),
        exp_status: req.body.exp_status,
        exp_handler: req.body.exp_handler,
        startDate: new Date(req.body.startDate),
        endDate: new Date(req.body.endDate),
        merchant: req.body.merchant,
        exp_rate:parseFloat(req.body.exp_rate),
        approved_on: approvedDate?new Date(approvedDate) : null,
        business_id: req.body.business_id,
        synced: true, 
        closed:false //for deletion purposes  
    }
    
    //Save the product in the database.
    Expense.create(newExpense)
        .then(data=>{
            res.status(201).send({
              success:true,
              message: data
            })
        })
        .catch(err=>{
            res.status(500).send({
                message:
                  err.message || "Some error occurred while creating the Expense."
            });
        })
}

// Update a Expense by the id in the request
exports.updateExpense = (req, res) => {
    
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const id = req.params.id,
          updateItemBody = req.body.updatedFields,
          updateObject = {
            expense_description: updateItemBody.expense_description,
            exp_amount: parseFloat(updateItemBody.exp_amount),
            exp_status: updateItemBody.exp_status,
            startDate:  new Date(updateItemBody.startDate),
            endDate:  new Date(updateItemBody.endDate),
            merchant: updateItemBody.merchant,
            exp_rate: parseFloat(updateItemBody.exp_rate),
            approved_on: isValidDate(updateItemBody.approved_on)? new Date(updateItemBody.approved_on) : null
          };
    debugger;
    Expense.update(updateObject,{
        where:{id:id}   
    })
    .then(num => {
        if (num == 1) {

          this.findExpenseById(id)
               .then(expense=>{
                  res.status(200).send({
                    success: true,
                    message: expense
                  });
               })
               .catch(err=>{
                  res.status(200).send({
                     success: false,
                     message: `Failed to find the expense with id=${id}`
                  });
               })
        } else {
            res.send({
               success: false,
               message: `Cannot update Expense with id=${id}. Maybe Expense was not found or req.body is empty!`
            });
        }
       })
       .catch(err => {
          res.status(500).send({
            success: false,
            message: "Error updating Expense with id=" + id
          });
        });
};  

//Retrieve all Expenses/ find by shopId from the database:
exports.findAllExpenses = (req, res) => {
  const shopId = req.query.shopId //to get query string from the Request
  var condition =  shopId ? { business_id: {[Op.like]:`%${shopId}%`}, closed:false} : null
  Expense.findAll({ where: condition, raw:true })
        .then(data => {
            res.status(200).send({
              success: true, 
              expenses:data
            });
        })
        .catch(err => {
            res.status(500).send({
               message:
                   err.message || "Some error occurred while retrieving expenses"
            });
        });
};

exports.deleteExpenseItem = (req,res)=>{
  //just close the expenseItem.
  const expenseId = req.params.id,
        updateObj = {
          closed:true
        }
  
   Expense.update(updateObj, {
          where: {id: expenseId}
      })
      .then(num=>{
          if (num == 1) {
              res.send({
                success: true,
                message: "ExpenseItem was deleted successfully!"
              });
          } else {
            res.send({
               success: false,
               message: `Cannot delete ExpenseItem with id=${id}. Maybe ExpenseItem was not found!`
            });
          }    
      })
      .catch(err=>{
          res.status(500).send({
             success: false,
             message: "Could not delete ExpenseItem with id=" + id
          });
      })

}

exports.findExpenseById = (expenseId)=>{
  return new Promise((resolve,reject)=>{
    Expense.findByPk(expenseId)
        .then(result=>{
          resolve(result)
        })
        .catch(err=>{
          reject(err)
        })
    })
}

//==>Expense Category.
exports.findAllExpenseCategory = (req, res) => {
  const shopId = req.query.shopId //to get query string from the Request
  var condition =  shopId ? { cat_shopId: {[Op.like]:`%${shopId}%`}, closed:false} : null
   
  const defaultCategories = getDefaultExpenseCategory()
  let finalCategories = []
  ExpenseCategory.findAll({ where: condition, raw:true })
        .then(data => {
            finalCategories = [...data, ...defaultCategories]
            res.status(200).send({
              success: true, 
              expenseCategories: finalCategories
            });
        })
        .catch(err => {
            res.status(500).send({
               message:
                   err.message || "Some error occurred while retrieving expense categories"
            });
        });
};


exports.createExpenseCategory  = (req,res)=>{

  const errors = validationResult(req)
  
  if (! errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
  }
  
  const newCategory = {
      cat_name: req.body.cat_name,
      cat_shopId: req.body.cat_shopId,
      synced: true, 
      closed:false //for deletion purposes  
  }
  
  ExpenseCategory.create(newCategory)
      .then(data=>{
          res.status(201).send({
            success:true,
            message: data
          })
      })
      .catch(err=>{
          res.status(500).send({
              message:
                err.message || "Some error occurred while creating the Expense."
          });
      })
}

exports.updateExpenseCategory = (req, res) => {
    
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
  }
  
  const id = req.params.id,
        updateItemBody = req.body.updatedFields,
        updateObject = {
           cat_name: req.body.cat_name
        };
  
  ExpenseCategory.update(updateObject,{
      where:{id:id}   
  })
  .then(num => {
      if (num == 1) {

        this.findExpenseById(id)
             .then(expense=>{
                res.status(200).send({
                  success: true,
                  message: expense
                });
             })
             .catch(err=>{
                res.status(200).send({
                   success: false,
                   message: `Failed to find the expense category with id=${id}`
                });
             })
      } else {
          res.send({
             success: false,
             message: `Cannot update Expense category with id=${id}. Maybe Expense category was not found or req.body is empty!`
          });
      }
     })
     .catch(err => {
        res.status(500).send({
          success: false,
          message: "Error updating Expense category with id=" + id
        });
      });
}; 

exports.findExpenseCategoryById = (categoryId)=>{
  return new Promise((resolve,reject)=>{
    ExpenseCategory.findByPk(categoryId)
        .then(result=>{
          resolve(result)
        })
        .catch(err=>{
          reject(err)
        })
    })
}


exports.deleteExpenseCategory = (req,res)=>{
  //just close the expenseItem.
  const categoryName = req.params.id,
        updateObj = {
          closed:true
        }
  
   ExpenseCategory.update(updateObj, {
          where: {cat_name: categoryName}
      })
      .then(num=>{
          if (num == 1) {
              res.send({
                success: true,
                message: "Expense category was deleted successfully!"
              });
          } else {
            res.send({
               success: false,
               message: `Cannot delete Expense category with id=${id}. Maybe Expense category was not found!`
            });
          }    
      })
      .catch(err=>{
          res.status(500).send({
             success: false,
             message: "Could not delete Expense category with id=" + id
          });
      })

}

////handle the metrics for totalExpenses by category.
exports.pickTotalExpensesByCategory = (req,res)=>{
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
     return res.status(422).json({ errors: errors.array() })
  }

  const startDate = new Date(req.body.startDate),
          endDate = new Date(req.body.endDate)
  const shopId = req.body.shopId //to get query string from the Request
  var condition =  shopId ? 
                      {
                        business_id: {[Op.like]:`%${shopId}%`}, 
                        closed: false,
                        exp_status:{[Op.ne]: 'rejected'},
                        startDate: {[Op.gte]: startDate},
                        endDate: {[Op.lte]: endDate}
                      }
                      : 
                    null;
  
      Expense.findAll({where: condition})
          .then(filteredExpenses =>{
               this.groupExpensesByCategory(filteredExpenses, shopId)
                   .then(result=>{
                        res.status(200).send({
                           success: true,
                           message: result
                        })
                   })
                   .catch(err=>{
                      res.status(200).send({
                        success: false,
                        message:
                            err.message || "Errors while grouping the expenses metrics"
                      })
                   })
        })
        .catch(err=>{
            res.status(500).send({
              success: false,
              message:
                  err.message || "Some error occurred while retrieving totalExpenses"
            });
       })
    
}

exports.groupExpensesByCategory = (expenses,shopId)=>{
    return new Promise((resolve,reject)=>{
        let groupedExp = []
        const defaultCategories = getDefaultExpenseCategory()
        let finalCategories = []
        var condition =  shopId ? 
                      {
                        cat_shopId: {[Op.like]:`%${shopId}%`}, 
                        closed: false
                      }
                      : 
                    null;
        ExpenseCategory.findAll({ where: condition, raw:true })
            .then(data => {
                  finalCategories = [...data, ...defaultCategories]

                  finalCategories.forEach(item=>{
                      let filteredExp = expenses.filter(exp=> exp.exp_category === item.cat_name)
                      groupedExp.push({
                         expCategory: item,
                         totalAmount: getTotalExpenses(filteredExp)//filteredExp is empty []
                      })
                  })
                  resolve(groupedExp)
            })
            .catch(err => {
                reject(err)
            });
    })
}


