/*
* SALEORDER CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const saleOrder = db.saleOrder
const Product = db.products
const Op = db.Sequelize.Op

const { updateProductAfterSaleOrder } = require('./product.controller')
const { createNewCreditPayment, getTotalCreditPayments } = require('./creditPayment.controller')

const { calculateTotalSaleOrders } = require('../utils/common')



exports.createSaleOrder  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    const products = req.body.products
    const newSaleOrder = {
       totalAmount: parseFloat(req.body.totalAmount),
       paymentOption: req.body.paymentOption,
       client: req.body.client,
       products: JSON.stringify(products),//allow string not array
       depositAmount: parseFloat(req.body.depositAmount),
       order_date: new Date(req.body.order_date),
       shipped_date: new Date(req.body.shipped_date),
       status: req.body.status,
       actor: req.body.actor,
       shopId: req.body.shopId,
       synced: true, 
       closed:false //for deletion purposes     
    }

    //Save the saleOrder in the database.
    saleOrder.create(newSaleOrder)
        .then(data=>{

            updateProductAfterSaleOrder(products)
               .then(result=>{
                    console.log('updated product after saleOrder', result)
                    
                     //add support for creditPayment deposit here
                    if (newSaleOrder.paymentOption === 'credit') {
                        this.creditPaymentForSaleOrder(data)
                            .then(result=>{
                               console.log('sucessful recorded credit deposit', result)
                            })
                            .catch(err=>{
                               console.log('failed to record credit payment', err)
                            }) 
                    }
                    res.status(201).send({
                        success:true,
                        message:data
                    })
                         
               })
               .catch(err=>{
                    res.status(422).send({
                      success: false,
                      message:
                        err.message || "Some error occurred while creating the saleOrder"
                    });
               })
           

        })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message:
                  err.message || "Some error occurred while creating the saleOrder."
            });
        })
}

//Retrieve all saleOrder for that particular shopId from the database.
exports.findAllSaleOrders = (req, res) => {

    const shopId = req.query.shopId //to get query string from the Request
    var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed: false} : null,
        orderFormat = [
            ['order_date', 'DESC']
        ]
    saleOrder.findAll({ where: condition, order:orderFormat })
         .then(data => {
            res.status(200).send({
               success: true, 
               orderList: data
            });
         })
         .catch(err => {
              res.status(500).send({
                 success: false,
                 message:
                   err.message || "Some error occurred while retrieving saleOrder"
              });
         });
};


  // Update a saleOrderItem by the id in the request
exports.updateSaleOrderItem = (req, res) => {
    
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const id = req.params.id,
          updateItemBody = req.body.updatedFields,
          updateObject = {
            products: JSON.stringify(updateItemBody.products),
            totalAmount: parseFloat(updateItemBody.totalAmount),
            status: updateItemBody.status,
            shipped_date: new Date(updateItemBody.shipped_date),
            //batchCode: updateItemBody.batchCode,
          },
          productUpdates = req.body.productUpdates,
          removeProducts = req.body.removeProducts

    saleOrder.update(updateObject,{
       where:{referenceId:id}   
    })
    .then(num => {
        if (num == 1) {
            
          //purpose of updating the productStock using productInfo 
            if (productUpdates.length > 0) {
                updateProductAfterSaleOrder(productUpdates, updateObject.status )
                    .then(result=>{
                        console.log('updated productItems for existing items in saleOrder', result)
                    })
                    .catch(err=>{
                        console.log('Failed to update the productItems for existing items in saleOrder', err)
                    })
            }

            if (removeProducts.length > 0) {
                let deleteProducts = removeProducts
                deleteProducts.forEach(item=>{
                    item.productQty = -parseFloat(item.productQty) //so that we can reuse below method
                })

                updateProductAfterSaleOrder(deleteProducts, updateObject.status)
                    .then(result=>{
                        console.log('updated the productItems after they have been removed in saleOrder', result) 
                    })
                    .catch(err=>{
                        console.log('failed to update productItems after they have been removed in saleOrder', err)
                    })
            }

            //u need to query for saleOrderItem by Id n push it in mobx
             setTimeout(() => {
                 this.findSaleOrderById(id)
                     .then(orderObj=>{
                        res.status(200).send({
                            success:true,
                            message: orderObj
                        })
                     })
                     .catch(err=>{
                        res.status(412).send({
                            success: false,
                            message: 'Failed to retrieve the updated saleOrderItem'
                        })
                     })
             }, 2000);

        } else {
          res.status(200).send({
            success: false,
            message: `Cannot update saleOrderItem with id=${id}. Maybe saleOrderItem was not found or req.body is empty!`
          });
        }
       })
       .catch(err => {
          res.status(500).send({
            success: false,
            message: "Error updating saleOrder with id=" + id
        });
    });

};


//Retrieve a single saleOrder with products info.
exports.findSaleOrderById = (saleId)=>{
    return new Promise((resolve,reject)=>{
        saleOrder.findByPk(saleId)
            .then(result=>{
                resolve(result)
            })
            .catch(err=>{
                reject(err)
            })
    })
}

exports.deleteSaleOrderItem = (req,res)=>{
    //just close the saleItem.
    const saleId = req.params.id,
          updateObj = {
            closed:true
          }
    debugger;
    saleOrder.update(updateObj, {
            where: {referenceId: saleId}
        })
        .then(num=>{
            if (num == 1) {
              res.send({
                 success: true,
                 message: "SaleOrderItem was deleted successfully!"
              });
            } else {
              res.send({
                success: false,
                message: `Cannot delete SaleOrderItem with id=${id}. Maybe SaleOrderItem was not found!`
              });
            }    
        })
        .catch(err=>{
            res.status(500).send({
               success: false,
               message: "Could not delete SaleOrderItem with id=" + id
            });
    })

}


exports.pickTotalSaleOrder = (req,res)=>{
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
       return res.status(422).json({ errors: errors.array() })
    }

    const startDate = new Date(req.body.startDate),
            endDate = new Date(req.body.endDate)
    const shopId = req.body.shopId //to get query string from the Request
    var condition =  shopId ? 
            {
                shopId: {[Op.like]:`%${shopId}%`}, 
                closed: false,
                order_date: {[Op.between]: [startDate, endDate]}
            }
                : 
            null;
    
    saleOrder.findAll({where: condition})
        .then(filteredSaleOders =>{
                const totalOrders = calculateTotalSaleOrders(filteredSaleOders)
                res.status(200).send({
                    success: true,
                    message: totalOrders
                })
        })
        .catch(err=>{
              res.status(500).send({
                success: false,
                message:
                    err.message || "Some error occurred while retrieving saleOrders"
              });
        })
      
}


///sale order metrics for credit purposes.
exports.getTotalCreditSaleOrders = (input)=>{
    return new Promise((resolve,reject)=>{
        const startDate = new Date(input.startDate),
              endDate = new Date(input.endDate),
              shopId = input.shopId //to get query string from the Request
        var condition =  shopId ? 
                        {
                          shopId: {[Op.like]:`%${shopId}%`}, 
                          closed: false,
                          paymentOption: 'credit',
                          order_date: {[Op.between]: [startDate, endDate]}
                        }
                          : 
                        null;
        saleOrder.findAll({where:condition})
                .then(filteredCreditSales =>{
                    const creditSaleOrders = calculateTotalSaleOrders(filteredCreditSales)
                    resolve(creditSaleOrders)
                })
                .catch(err=>{
                    reject(err)
                })
      
       })
  }

//calculate the creditPayments debt for sales.
exports.saleOrderCreditDebt = (req,res)=>{
    return new Promise((resolve,reject)=>{

        const inputDate = {
            startDate: req.body.startDate,
            endDate: req.body.endDate,
            shopId: req.body.shopId
         }
        this.getTotalCreditSaleOrders(inputDate)
            .then(result=>{
                getTotalCreditPayments(inputDate, 'sale-order')
                    .then(totalDeposit=>{
                      const metrics = {
                        saleOrderCredit:result,
                        amountPaid: totalDeposit
                      }
                      res.status(200).send({
                        success: true,
                        message: metrics
                      })
                })
            })
            .catch(err=>{
                res.status(500).send({
                   success: false,
                   message: err.message | "Error in generating the total credit sales metrics"
                })
            })
    }) 
}

exports.creditPaymentForSaleOrder = (newOrder)=>{
    return new Promise((resolve,reject)=>{
        const depositAmount = parseFloat(newOrder.depositAmount),
              originalDebt =  parseFloat(newOrder.totalAmount)

        const newDeposit = {
            referenceId: newOrder.referenceId,
            category: 'sale-order',
            contactId: newOrder.client,
            depositAmount: depositAmount,
            originalDebt: originalDebt,
            balance: originalDebt - depositAmount,
            note: "The sale-order was extended via credit",
            soldAt: new Date(newOrder.order_date) || new Date(),
            closed: false,
            shopId: newOrder.shopId,
            synced: true
        }

        createNewCreditPayment(newDeposit)
            .then(result=>{
               resolve(result)
            })
            .catch(err=>{
                reject(err)
            })

    })
}

/**
 * SKIPPED THESE METHODS.
 * - topSellingItems for the sale Orders (Not critical but needed)
 *  it is not also implemented for single user
 */
  