/*
* PRODUCT CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const Product = db.products
const ProductCategory = db.productCategories
const ProductBrand = db.productBrand
const Op = db.Sequelize.Op //holds all the operator symbols.


//Create and save the new Product.
exports.createProduct  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
   //Create the product
    const product = {
       prod_name: req.body.prod_name,
       prod_code: req.body.prod_code,
       prod_description: req.body.prod_description,
       unit_price: req.body.unit_price,
       retail_price: req.body.retail_price,
       orig_stock: req.body.orig_stock,
       reorder_level: req.body.reorder_level,
       prod_category: req.body.prod_category,
       expiry_date: req.body.expiry_date,
       batchEnabled: req.body.batchEnabled,
       qty_sold: req.body.qty_sold,
       brand_name: req.body.brand_name,
       standard_measure_unit: req.body.standard_measure_unit,
       piece_measure_unit: req.body.piece_measure_unit,
       piece_unit_price: parseFloat(req.body.piece_unit_price) || 0,
       standard_piece_ratio_form1:req.body.standard_piece_ratio_form1,
       standard_piece_ratio_form2:req.body.standard_piece_ratio_form2,
       discount_rate: parseFloat(req.body.discount_rate),
       shopId: req.body.shopId,
       synced: true, //when not connected to the internet
       closed:false //for deletion purposes     
    }

    //Save the product in the database.
    Product.create(product)
        .then(data=>{
           res.status(201).send({
              success:true,
              message: data
           })
        })
        .catch(err=>{
            res.status(500).send({
                message:
                  err.message || "Some error occurred while creating the Product."
            });
        })
}

// Update a Product by the id in the request
exports.updateProduct = (req, res) => {
    
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const id = req.params.id,
          updateObject = {
            prod_code: req.body.prod_code,
            prod_description: req.body.prod_description,
            unit_price: req.body.unit_price,
            retail_price: req.body.retail_price,
            reorder_level: req.body.reorder_level,
            prod_category: req.body.prod_category,
            expiry_date: req.body.expiry_date,
            batchEnabled: req.body.batchEnabled,
            brand_name: req.body.brand_name,
            standard_measure_unit: req.body.standard_measure_unit,
            piece_measure_unit: req.body.piece_measure_unit,
            piece_unit_price: parseFloat(req.body.piece_unit_price) || 0,
            standard_piece_ratio_form1:req.body.standard_piece_ratio_form1,
            standard_piece_ratio_form2:req.body.standard_piece_ratio_form2,
            discount_rate: parseFloat(req.body.discount_rate),
          };
    
    Product.update(updateObject,{
       where:{id:id}   
    })
    .then(num => {
        if (num == 1) {
          res.send({
            success: true,
            message: "Product was updated successfully."
          });
        } else {
          res.send({
            success: false,
            message: `Cannot update Product with id=${id}. Maybe Product was not found or req.body is empty!`
          });
        }
       })
       .catch(err => {
          res.status(500).send({
            success: false,
            message: "Error updating Product with id=" + id
          });
        });
};  
    

    // Delete a Product with the specified id in the request
    //Just close it like the way u r doing for offline way
exports.deleteProduct = (req, res) => {
    
    const id = req.params.id;
    //check if still attached to new-stock, sales, sale-order or returnItems.
    //only close it if the above are checked.
    Product.destroy({
      where: { id: id }
    })
    .then(num => {
        if (num == 1) {
            res.send({
              message: "Product was deleted successfully!"
           });
        } else {
            res.send({
              message: `Cannot delete Product with id=${id}. Maybe Product was not found!`
            });
        }
    })
    .catch(err => {
      res.status(500).send({
        success: false,
        message: "Could not delete Product with id=" + id
      });
    });
};

// Retrieve all Products for that particular shopId from the database.
exports.findAllProducts = (req, res) => {
    //Retrieve all Products/ find by shopId from the database:
     const shopId = req.query.shopId //to get query string from the Request
     var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed:false} : null,
        orderFormat = [
           ['createdAt', 'DESC']
        ]
      Product.findAll({ where: condition, order: orderFormat })
         .then(data => {
           res.status(200).send({
             success: true, 
             products: data
           });
         })
         .catch(err => {
           res.status(500).send({
              message:
               err.message || "Some error occurred while retrieving Products."
           });
         });
 };

//not catering for synchronisation purposes. focus on johnson street shopping mall requirements

//PRODUCT CATEGORIES.
exports.createProductCategory = (req,res)=>{
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
       return res.status(422).json({ errors: errors.array() })
    }

    const newCategory = {
      cat_name: req.body.cat_name,
      cat_shopId: req.body.cat_shopId,
      synced: false,
      closed: false
    }

    //Save the product in the database.
    ProductCategory.create(newCategory)
        .then(data=>{
           res.status(201).send({
              success:true,
              message: data
           })
        })
        .catch(err=>{
            res.status(500).send({
                message:
                  err.message || "Some error occurred while creating the Product Category."
            });
        })
}

exports.findAllProductCategory = (req, res) => {
  //Retrieve all Products/ find by shopId from the database:
   const shopId = req.query.shopId //to get query string from the Request
   var condition =  shopId ? {cat_shopId: {[Op.like]:`%${shopId}%`}, closed:false} : null
   ProductCategory.findAll({ where: condition })
       .then(data => {
         res.status(200).send({
           success: true, 
           categories: data
         });
       })
       .catch(err => {
         res.status(500).send({
            message:
             err.message || "Some error occurred while retrieving categories"
         });
       });
};

// Update a Product Category by the id in the request
exports.updateProductCategory = (req, res) => {
    
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
  }
  
  const id = req.params.id,
        updateObject = {
          cat_name: req.body.cat_name
        };

  ProductCategory.update(updateObject,{
     where:{id:id}   
  })
  .then(num => {
      if (num == 1) {
        res.send({
          success: true,
          message: "Product Category was updated successfully."
        });
      } else {
        res.send({
          success: false,
          message: `Cannot update Product Category with id=${id}. Maybe Product Category was not found or req.body is empty!`
        });
      }
     })
     .catch(err => {
        res.status(500).send({
          success: false,
          message: "Error updating Product Category with id=" + id
        });
      });
  };


//Just close it like the way u r doing for offline way
exports.deleteProductCategory = (req, res) => {
    
     const categoryName = req.params.id;
     //check if still attached to product
      Product.findOne({ where: {prod_category: categoryName} })
          .then(result=>{
              if ( !result || result.length === 0) {
                  ProductCategory.destroy({
                      where: { cat_name: categoryName }
                  })
                    .then(num => {
                      if (num == 1) {
                          res.send({
                             success: true,
                             message: "Product Category was deleted successfully!"
                          });
                      } else {
                          res.send({
                              success: false,
                              message: `Cannot delete Product Category with id=${categoryName}. Maybe Product Category was not found!`
                          });
                      }
                  })
                  .catch(err => {
                      res.status(500).send({
                        success: false,
                        message: "Could not delete Product with id=" + categoryName
                      });
                  });
                
              } else {
                   //just close the categoryName if it is still attached to the product
                      const updateObject = {
                        closed: true
                      };
                      ProductCategory.update(updateObject, {
                         where:{cat_name: categoryName}
                      })
                       .then(num=>{
                          res.send({
                             success: true,
                             message: "Product Category was closed successfully!"
                          });
                       })
                        .catch(err=>{
                            res.send({
                              success: false,
                              message: `Cannot close Product Category with name=${categoryName}. Maybe Product Category was not found!`
                            });
                        })
              } 
          })
           .catch(err=>{
               res.status(500).send({
                   success: false,
                   message: "Error deleting  Product Category with id=" + categoryName
              }); 
           }); 
  }

  
  /****PRODUCT BRANDITEM ***/
exports.createProductBrand = (req,res)=>{
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
     return res.status(422).json({ errors: errors.array() })
  }

  const newBrandItem = {
     brand_name: req.body.brand_name,
     brand_shopId: req.body.brand_shopId,
     synced: false,
     closed: false
  }

  //Save the product brandItem in the database.
  ProductBrand.create(newBrandItem)
      .then(data=>{
         res.status(201).send({
            success:true,
            message: data
         })
      })
      .catch(err=>{
          res.status(500).send({
              success: false,
              message:
                err.message || "Some error occurred while creating the Product Brand."
          });
      })
}

//Retrieve all Product brands/ find by shopId from the database:
exports.findAllProductBrand = (req, res) => {
   const shopId = req.query.shopId //to get query string from the Request
   var condition =  shopId ? { brand_shopId: {[Op.like]:`%${shopId}%`}, closed:false} : null
   ProductBrand.findAll({ where: condition })
       .then(data => {
         res.status(200).send({
           success: true, 
           brands: data
         });
       })
       .catch(err => {
         res.status(500).send({
            message:
             err.message || "Some error occurred while retrieving product brands"
         });
       });
};

// Update a Product Brand by the id in the request
exports.updateProductBrand = (req, res) => {
    
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
  }
  
  const id = req.params.id,
        updateObject = {
          brand_name: req.body.brand_name
        };

  ProductBrand.update(updateObject,{
     where:{id:id}   
  })
  .then(num => {
      if (num == 1) {
        res.send({
          success: true,
          message: "Product Brand was updated successfully."
        });
      } else {
        res.send({
          success: false,
          message: `Cannot update Product Brand with id=${id}. Maybe Product Brand was not found or req.body is empty!`
        });
      }
     })
     .catch(err => {
        res.status(500).send({
          success: false,
          message: "Error updating Product Brand with id=" + id
        });
      });
  };


  //Just close it like the way u r doing for offline way
exports.deleteProductBrand = (req, res) => {
    
  const brandName = req.params.id;
 
  //check if still attached to product
   Product.findOne({ where: {brand_name: brandName} })
       .then(result=>{
           if (!result) { //result === null
               ProductBrand.destroy({
                   where: { brand_name: brandName }
               })
                 .then(num => {
                   if (num == 1) {
                       res.send({
                          success: true,
                          message: "Product Brand was deleted successfully!"
                       });
                   } else {
                       res.send({
                           success: false,
                           message: `Cannot delete Product Brand with name=${brandName}. Maybe Product Brand was not found!`
                       });
                   }
               })
               .catch(err => {
                   res.status(500).send({
                     success: false,
                     message: "Could not delete Product Brand with name=" + brandName
                   });
               });
             
           } else {
                //just close the brandName if it is still attached to the product
                   const updateObject = {
                     closed: true
                   };
                   ProductBrand.update(updateObject, {
                      where:{brand_name: brandName}
                   })
                    .then(num=>{
                       res.send({
                          success: true,
                          message: "Product Brand was closed successfully!"
                       });
                    })
                     .catch(err=>{
                         res.send({
                           success: false,
                           message: `Cannot close Product Brand with name=${brandName}. Maybe Product Category was not found!`
                         });
                     })
           } 
       })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message: "Error deleting  Product Brand with name=" + brandName
           }); 
        }); 
}

//Update the product orig-stock after the  newStock
exports.updateProductAfterStocklist = (newStock)=>{
    return new Promise((resolve,reject)=>{
        Product.increment('orig_stock',{by: parseFloat(newStock.sales_qty), where:{id: newStock.productId}})
            .then(result=>{
              resolve(result)
            })
            .catch(err=>{
              reject(err)
            })
    })
}

//Update the product orig-stock after the  newSale
exports.updateProductAfterSale = (newSale)=>{
  return new Promise((resolve,reject)=>{
      Product.decrement('orig_stock',{by: parseFloat(newSale.sales_qty), where:{id: newSale.productId}})
          .then(result=>{
            resolve(result)
          })
          .catch(err=>{
            reject(err)
          })
  })
}

//Reduce the stock level when customer takes away product.
exports.updateProductAfterPOSDetail = (posItems)=>{
    return new Promise((resolve,reject)=>{
        posItems.forEach((item,i)=>{
            Product.decrement('orig_stock',{by: parseFloat(item.productQty), 
                where:{ prod_name: item.productName }})
                .then(result=>{
                    Product.increment('qty_sold', {by: parseFloat(item.productQty),
                       where: { prod_name: item.productName }
                    })
                    .then(result=>{
                       console.log(result)
                    })
                    .catch(err=>{
                       console.log(err)
                    })
                })
                .catch(err=>{
                   console.log(err)
                })
                
            if (i === (posItems.length - 1)) {
                resolve('Updated the product after POSDetail')
            }
        })
    })
}

exports.updateProductAfterSaleOrder = (productItems,orderStatus)=>{
    return new Promise((resolve,reject)=>{
        const checkOrderStatus = orderStatus === 'paid'||orderStatus==='shipped'||orderStatus=== 'credit'
         
        productItems.forEach((item,i)=>{
             //only make changes if  orderStatus => 'credit', 'paid', 'shipped' 
            Product.decrement('orig_stock',{by: checkOrderStatus? parseFloat(item.productQty): 0, 
              where:{ prod_name: item.productName }})
              .then(result=>{
                  Product.increment('qty_sold', {by: checkOrderStatus? parseFloat(item.productQty):0,
                     where: { prod_name: item.productName }
                  })
                  .then(result=>{
                      console.log(result)
                  })
                  .catch(err=>{
                      console.log(err)
                  })
              })
              .catch(err=>{
                 console.log(err)
              })
            
              if (i === (productItems.length - 1)) {
                  resolve('product updated  after the sale order')
              }
        })
    })
}


exports.updateProductAfterPurchaseOrder = (productItems,orderStatus)=>{
  return new Promise((resolve,reject)=>{
      const checkOrderStatus = orderStatus === 'received'||orderStatus==='ordered'||orderStatus=== 'credit' 
       
      productItems.forEach((item,i)=>{
          Product.increment('orig_stock',{by: checkOrderStatus? parseFloat(item.productQty):0, 
            where:{ prod_name: item.productName }})
            .then(result=>{
                console.log(result)
            })
            .catch(err=>{
               console.log(err)
            })

          if (i === (productItems.length - 1) ) {
             resolve('Updated the product after the purchase Order')
          }
      })
  })
}


// these are left methods that u need to add .
/*
  //nedb_offline
  - handleImportProduct(products)  -> //handling the bulk import of products.
  - updateProductAfterReturn(productItems,returnType)
*/

  