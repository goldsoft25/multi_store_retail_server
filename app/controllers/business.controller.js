/*
* BUSINESS CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const Employee = db.Employee
const Op = db.Sequelize.Op

const {generatePasswordSalt, encryptUserPassword} = require('../utils/common')
const { createUserToken } = require('../utils/jwtTokenMiddleware')

exports.createNewEmployee  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
    
    const newEmployee = {
        username: req.body.emp_username,
        password: req.body.emp_password,
        role: req.body.emp_role,
        business_id: req.body.biz_code,
        status: req.body.emp_status,
        synced: true, 
        closed:false //for deletion purposes  
    }

    //handle passwords here since sequelize hooks are disturbing
    newEmployee.salt = generatePasswordSalt()
    newEmployee.password = encryptUserPassword(newEmployee.password, newEmployee.salt)
    
    this.checkDuplicateUsernameOrEmail(newEmployee.username)
        .then(result=>{
            //Save the employee in the database.
            Employee.create(newEmployee)
                .then(data=>{
                    res.status(201).send({
                       success:true,
                       message: data
                    })
                })
                .catch(err=>{
                   res.status(500).send({
                   message:
                      err.message || "Some error occurred while creating the Employee."
                });
            })
        })
        .catch(err=>{
            res.status(400).send({
                success: false, 
                message: "Failed! Username is already in use!"
            });
        })
}


//check if username or email is duplicate or not
exports.checkDuplicateUsernameOrEmail = (username)=>{

    return new Promise((resolve,reject)=>{   
         // Username
        Employee.findOne({
            where: {
              username: username
            }
        }).then(user => {
            if (user) {
                reject('Username is already registered')
            }else{
                resolve('continue with registration')
            }
        })
        .catch(err=>{
            reject(err)
        })
    })
    
}


//Retrieve all Employees/ find by shopId from the database:
exports.findAllEmployees = (req, res) => {
    const shopId = req.query.shopId //to get query string from the Request
    var condition =  shopId ? { business_id: {[Op.like]:`%${shopId}%`}, closed:false} : null,
        orderFormat = [
          ['createdAt', 'DESC']
        ]
    Employee.findAll({ where: condition, order:orderFormat, raw:true })
        .then(data => {
           res.status(200).send({
              success: true, 
              employees: data
            });
        })
        .catch(err => {
           res.status(500).send({
              message:
                err.message || "Some error occurred while retrieving employees"
            });
        });
};

exports.loginAsEmployee = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }

    const username = req.body.username,
          userPassword = req.body.password
    
    Employee.findOne({
        where: {
          username: username
        },
        raw:true
    }).then(user => {
        if (user) {
            const userPasswordHash = encryptUserPassword(userPassword,user.salt)
            if (userPasswordHash  ===  user.password) {
                res.status(200).send({
                    success:true,
                    user:user,
                    token:createUserToken(user.employeeId) 
                })
            } else {
                res.status(422).send({
                    success: false,
                    message: 'Invalid business email/password combination'
                }) 
            }
        }else{
            res.status(404).send({
                success:false,
                message: `That employee with that username ${username} is not registered `
            })
        }
    })
    .catch(err=>{
        res.status(500).send({
            success: false, 
            message: 
                err.message || "Some error occurred while retrieving employee detail"
        })
    })
}
