/*
* SALES CONTROLLER
* handles the business logic and also interacts with models.
* larger projects.. it is the service that interacts with the database models
* while controller hooks into the services for the data access.
*/

const { validationResult } = require('express-validator')
const db = require('../models');
const Sales = db.sales
const Products = db.products
const Op = db.Sequelize.Op

const { updateProductAfterSale } = require('./product.controller')
const { createNewCreditPayment, getTotalCreditPayments } = require('./creditPayment.controller')

const { getTotalSales } = require('../utils/common')

//Create and save the new Sale.
exports.createNewSale  = (req,res)=>{

    const errors = validationResult(req)
    if (! errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() })
    }
   //Create the newSale
    const newSale = {
       prod_name: req.body.prod_name,
       retail_price: req.body.retail_price,
       costPrice: req.body.costPrice,
       sales_qty: req.body.sales_qty,
       who_sold: req.body.who_sold,
       batchCode: req.body.batchCode,
       paymentOption: req.body.paymentOption,
       depositAmount: parseFloat(req.body.depositAmount),
       customerId: req.body.customerId,
       soldAt: new Date(req.body.soldAt),
       shopId: req.body.shopId,
       synced: true, //when not connected to the internet
       closed:false //for deletion purposes     
    }

    //Save the product in the database.
    Sales.create(newSale)
        .then(data=>{
           const productStock = {
              productId: req.body.productId,
              sales_qty: newSale.sales_qty
           }
            
            updateProductAfterSale(productStock)
               .then(result=>{
                    console.log('updated product result', result)
                      
                    //add support for creditPayment deposit here
                    if (newSale.paymentOption === 'credit') {
                         let depositAmount = parseFloat(data.depositAmount),
                           originalDebt = parseFloat(data.retail_price * data.sales_qty)
                        const newDeposit = {

                           referenceId: data.referenceId,
                           category: 'sale',
                           contactId: data.customerId,
                           depositAmount:depositAmount,
                           originalDebt: originalDebt,
                           balance: originalDebt-depositAmount,
                           note: "The sale was extended via credit",
                           soldAt: new Date(data.soldAt) || new Date(),
                           closed: false,
                           shopId: data.shopId,
                           synced: true
                        }

                        createNewCreditPayment(newDeposit)
                             .then(result=>{
                                  res.status(201).send({
                                    success:true,
                                    message:data
                                  })
                             })
                             .catch(err=>{
                                 console.log('failed to record credit payment', err)
                                    res.status(201).send({
                                       success:false,
                                       message:'Failed to record the credit payments'
                                    })
                              })
                    }else{
                          res.status(201).send({
                            success:true,
                            message:data
                          })
 
                    }

               })
               .catch(err=>{
                    res.status(422).send({
                      success: false,
                      message:
                        err.message || "Some error occurred while creating the Sales."
                    });
               })
            
        })
        .catch(err=>{
            res.status(500).send({
                success: false,
                message:
                  err.message || "Some error occurred while creating the Sales."
            });
        })
}

// Retrieve all Sales for that particular shopId from the database.
exports.findAllSales = (req, res) => {
    //Retrieve all Sales/ find by shopId from the database:
    const shopId = req.query.shopId //to get query string from the Request
    var condition =  shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed: false} : null,
        orderFormat = [
           ['soldAt', 'DESC']
        ]
    Sales.findAll({ where: condition, order:orderFormat})
         .then(data => {
           //use the INNER JOIN with products db before sending results
            res.status(200).send({
               success: true, 
               sales: data
            });
         })
         .catch(err => {
              res.status(500).send({
                 success: false,
                 message:
                   err.message || "Some error occurred while retrieving Incoming Stock."
              });
         });
 };

 //Retrieve a single Sales with products info.
exports.findSalesById = (saleId)=>{
    return new Promise((resolve,reject)=>{
      Sales.findByPk(saleId)
          .then(result=>{
            resolve(result)
          })
          .catch(err=>{
            reject(err)
          })
      })
}

// Update a saleItem by the id in the request
exports.updateSaleItem = (req, res) => {
    
  const errors = validationResult(req)
  if (! errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
  }
  
  const id = req.params.id,
        updateItemBody = req.body.updateItem,
        updateObject = {
          sales_qty: updateItemBody.sales_qty,
          soldAt: new Date(updateItemBody.soldAt),
          //batchCode: updateItemBody.batchCode,
        };

  Sales.update(updateObject,{
     where:{id:id}   
  })
  .then(num => {
      if (num == 1) {
        //purpose of updating the productStock using productInfo
        const editSale = {
           productId: req.body.productStock.productId,
           sales_qty: req.body.productStock.changeStockQty
        }
        updateProductAfterSale(editSale)
           .then(result=>{

                this.findSalesById(id)
                      .then(sale=>{
                        res.status(200).send({
                            success:true,
                            message: sale
                        })
                      })
                      .catch(err=>{
                          res.status(412).send({
                            success: false,
                            message: 'Failed to retrieve the updated saleItem'
                          })
                      })
           })
           .catch(err=>{
              res.status(200).send({
                  success: false,
                  message:
                      err.message || "Some error occurred while updating the product details for the saleItem"
              });
           })
      } else {
        res.status(200).send({
          success: false,
          message: `Cannot update saleItem with id=${id}. Maybe saleItem was not found or req.body is empty!`
        });
      }
     })
     .catch(err => {
        res.status(500).send({
          success: false,
          message: "Error updating sale with id=" + id
        });
      });
  };


exports.deleteSaleItem = (req,res)=>{
    //just close the saleItem.
    const saleId = req.params.id,
          updateObj = {
            closed:true
          }
     Sales.update(updateObj, {
            where: {id: saleId}
        })
        .then(num=>{
            if (num == 1) {
              res.send({
                 success: true,
                 message: "SaleItem was deleted successfully!"
              });
            } else {
              res.send({
                success: false,
                message: `Cannot delete SaleItem with id=${id}. Maybe SaleItem was not found!`
              });
            }    
        })
        .catch(err=>{
            res.status(500).send({
               success: false,
               message: "Could not delete SaleItem with id=" + id
            });
        })

}

exports.pickTopSellingItems = (req,res)=>{
    const errors = validationResult(req)
    if (! errors.isEmpty()) {
       return res.status(422).json({ errors: errors.array() })
    }

    const startDate = new Date(req.body.startDate),
            endDate = new Date(req.body.endDate)
    const shopId = req.body.shopId //to get query string from the Request
    var condition =  shopId ? 
                        {
                          shopId: {[Op.like]:`%${shopId}%`}, 
                          closed: false,
                          soldAt: {[Op.between]: [startDate, endDate]}
                        }
                        : 
                      null;
    
    Sales.findAll({where: condition})
          .then(filteredSales =>{
              let shopId = req.body.shopId,
                  productCondition = shopId ? {shopId: {[Op.like]:`%${shopId}%`}, closed:false} : null 
                  
               this.groupSalesByProdName(filteredSales,productCondition)
                   .then(result=>{
                        res.status(200).send({
                          success: true,
                          message: result
                        })
                   })
                   .catch(err=>{
                        res.status(422).send({
                          success: false,
                          message:  err.message | "Failed to generate the topSellingItems metrics"
                        })
                   })
          })
          .catch(err=>{
              res.status(500).send({
                success: false,
                message:
                    err.message || "Some error occurred while retrieving sales"
              });
         })
      
}

exports.groupSalesByProdName = (sales, productCondition)=>{
    return new Promise((resolve,reject)=>{
        const groupedSales = []
        Products.findAll({where: productCondition})
            .then(products=>{
                products.forEach(item=>{
                  let filteredSales = sales.filter(sale => sale.prod_name === item.prod_name)
                  groupedSales.push({
                    prodName:item.prod_name,
                    totalSales: getTotalSales(filteredSales)
                  })
                })

                resolve(groupedSales)
            })
            .catch(err=>{
               reject(err)
            })
    })
}

///sale metrics for credit purposes.
exports.getTotalCreditSales = (input)=>{
    return new Promise((resolve,reject)=>{
        const startDate = new Date(input.startDate),
              endDate = new Date(input.endDate),
              shopId = input.shopId //to get query string from the Request
        var condition =  shopId ? 
                        {
                          shopId: {[Op.like]:`%${shopId}%`}, 
                          closed: false,
                          paymentOption: 'credit',
                          soldAt: {[Op.between]: [startDate, endDate]}
                        }
                          : 
                        null;
        Sales.findAll({where:condition})
             .then(filteredCreditSales =>{
                const totalCreditSales = getTotalSales(filteredCreditSales)
                resolve(totalCreditSales)
             })
             .catch(err=>{
                 reject(err)
             })
      
      })
  }

//calculate the creditPayments debt for sales.
exports.salesCreditDebt = (req,res)=>{
    return new Promise((resolve,reject)=>{
        
      const inputDate = {
         startDate: req.body.startDate,
         endDate: req.body.endDate,
         shopId: req.body.shopId
      }
        this.getTotalCreditSales(inputDate)
            .then(result=>{
                getTotalCreditPayments(inputDate, 'sale')
                    .then(totalDeposit=>{
                      const metrics = {
                         saleCredit:result,
                         amountPaid: totalDeposit
                      }
                      res.status(200).send({
                        success: true,
                        message: metrics
                      })
                })
            })
            .catch(err=>{
                res.status(500).send({
                   success: false,
                   message: err.message | "Error in generating the total credit sales metrics"
                })
            })
    }) 
}

/***
 * Skipped these..
 * - handling the bulk import for sales 
 *    function  handleImportSales(sales)
 */