const jwt = require('jsonwebtoken')
const { TOKEN_SECRET } = require('../config/extra.config')

function createUserToken(user){
    if (!user) {
       return 'No user object provided'
    }
    var token = jwt.sign({
        exp: Math.floor(Date.now()/1000) + (60*60*24*14), // employee - 2 weeks
        data: user
    }, TOKEN_SECRET)

    return token
 }


function isUserAuthenticated(req,res,next){
    // check header or url parameters or post parameters for token
    var token = req.headers['authorization'] || req.headers['Authorization'] || req.body.token || req.query.token || req.headers['x-access-token'];
    //decode the token.
    if (token) {
       // remove the Bearer from the token to avoid invalid json web token.
       token = token.replace(/^Bearer\s/, '');
       //// verifies secret and checks exp
       jwt.verify(token, TOKEN_SECRET, function(err,decoded){
           if (err) {
              return res.status(401).send({"success": false, "message": err})
           }
           req.decoded = decoded
           next()
       })

    } else {
       return res.status(403).send({
         "success": false,
         "message": 'No token provided....'
       })
    }

}


module.exports.createUserToken = createUserToken
module.exports.isUserAuthenticated = isUserAuthenticated