module.exports = {
    defaultContacts: [
        {
           contactId:'TA2IEQW81ouTi07J',
           firstName: 'Walk-In',
           lastName: 'Customer',
           contact_type: 'customer',
           primary_contact: '078XXXXXXX',
           email: 'walkin-customer@bizwatch.com',
           company_name: 'XXXXXXX',
           company_street: 'XXXXXXXXXX'
        },
        {
            contactId:'XC3IEZW51ouTi08J',
            firstName: 'Walk-In',
            lastName: 'Supplier',
            contact_type: 'supplier',
            primary_contact: '078XXXXXXX',
            email: 'walkin-supplier@bizwatch.com',
            company_name: 'XXXXXXX',
            company_street: 'XXXXXXXXXX'
         }
    ],

    getDefaultExpenseCategory: function(shopId){
        
        const defaultCategories = [
            'Utility-Bills','Office-Supplies', 'Transport', 'Accomodation', 'Employee-Salaries'
        ]
        const defaultList = []
        
        defaultCategories.forEach(item=>{
            defaultList.push({
              id:Math.random().toString(36).substring(2,15).toLowerCase(),
              cat_name:item.toLowerCase(),
              cat_shopId:shopId,
              synced: true,
              closed:false 
            })
        })

        return defaultList
    }


}