const os = require('os');
//const { sales } = require('../models');
const Crypto = require('crypto')


function getServerIP(){
    var ifaces = os.networkInterfaces();
    var values = Object.keys(ifaces).map(function(name) {
        return ifaces[name];
      });
      values = [].concat.apply([], values).filter(function(val){
        return val.family == 'IPv4' && val.internal == false;
      });
    
      return values.length ? values[0].address : '0.0.0.0'; 
}


/// calculating the total sales qty for each productName..
//nedb_offline/sales.js
function getTotalSales(sales){
  let sumQty = 0,
      totalPrice = 0,
      totalCostPrice = 0

  for (var i = 0; i < sales.length; i++) {
    let eachSaleQty = sales[i].sales_qty
        sumQty = sumQty + parseFloat(eachSaleQty)
        totalPrice = totalPrice + (parseFloat(sales[i].retail_price)*parseFloat(eachSaleQty))
        totalCostPrice = totalCostPrice + (parseFloat(sales[i].costPrice)*parseFloat(eachSaleQty))
  }
     //console.log('hey total cost', totalCostPrice)
  return {
    totalQty:sumQty,
    totalAmount:totalPrice,
    costOfSales: totalCostPrice
  }
}


//calculate the total amount in each receipt list
function getTotalPOSDetailSales(filteredReceipts){
    let totalSales = 0,
        totalCost = 0

    if (!Array.isArray(filteredReceipts)) {
        filteredReceipts = [] //avoid forEach. of undefined bugs
    }

    filteredReceipts.forEach((item)=>{
        totalSales = totalSales + parseFloat(item.totalAmount)
        //what about the costPrice for these items.
        totalCost =  totalCost + calculateTotalPOSDetailCost(item.receiptItems)

    })

    return {
      totalSales: totalSales,
      totalCost: totalCost
    }
}

function calculateTotalPOSDetailCost(receiptItems){
    let totalCost = 0
    receiptItems = JSON.parse(receiptItems) //since we are dealing with string JSON for receiptItems
    receiptItems.forEach(item=>{
      totalCost+=( parseFloat(item.costPrice) * parseFloat(item.productQty) )
    })
   return totalCost
}

function calculateTopSellerPOSDetails(posItemSales, productName){
    let totalSales = 0

    posItemSales.forEach(posItem=>{   
        if (isValidJSON(posItem.receiptItems)) {
          console.log('yeah it is valid JSON')
            posItem.receiptItems = JSON.parse(posItem.receiptItems)
            let foundProductItem = posItem.receiptItems.find(k=>k.productName === productName)
            if (foundProductItem) {
               totalSales+=parseFloat(foundProductItem.productQty)   
            } 
        }

    })

    return {
       productName: productName,
       totalSaleQty: totalSales
    }
}

function isValidJSON(str){
    try {
       return (JSON.parse(str) && !!str) 
    } catch (error) {
       return false
    }
}


function generateRandomId(size = 16) { //match nedb _id
  return Crypto
    .randomBytes(size)
    .toString('base64')
    .slice(0, size)
    .replace(/[^\w\s]/gi, 'k') //get rid of special characters in my custom id
}

//calculte the total sales v the costs.
function calculateTotalSaleOrders(filteredOrders){
     
  let totalRevenue = 0,
       totalCostPrice = 0
  for (let i = 0; i < filteredOrders.length; i++) {
        const item = filteredOrders[i];
        totalRevenue = totalRevenue + parseFloat(item.totalAmount)
        totalCostPrice = totalCostPrice + calcTotalCostFromProducts(item.products)
  }
  return {
      subRevenue: totalRevenue,
      subCosts: totalCostPrice,
      itemNumber:filteredOrders.length
  }
}

function calcTotalCostFromProducts(products){
    let totalCostPrice = 0
        products = Array.from(JSON.parse(products))
        products.forEach(item=>{
            totalCostPrice+=(item.costPrice*item.productQty)
        })
  return totalCostPrice
}

//calculte the total purchases v the costs.
function calculateTotalPurchases(filteredOrders){
  let totalCostPrice = 0
  for (let i = 0; i < filteredOrders.length; i++) {
      const item = filteredOrders[i];
      //totalCostPrice = totalCostPrice + ( parseFloat(item.unitPrice)*item.productQty )
      totalCostPrice = totalCostPrice + parseFloat(item.totalAmount) //assume products total = totalAmount
  }
  
  return {
      subCosts: totalCostPrice,
      itemNumber:filteredOrders.length
  }
}

function isValidDate(date){
  return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
}

 //calculate teh total of the expense amounts.
function getTotalExpenses(expenses){
    let sum = 0
    for (let i = 0; i < expenses.length; i++) {
        sum = sum + parseFloat(expenses[i].exp_amount)
    }
  return sum
}

function calculateTotalDeposit(creditPayments){
  let total = 0
  creditPayments.forEach(item=>{
       total+=parseFloat(item.depositAmount)
  })
  return total
}

function generatePasswordSalt(){
   return Crypto.randomBytes(16).toString('base64')
}

function encryptUserPassword(plainText, salt){
   return Crypto.createHash('RSA-SHA256')
                .update(plainText)
                .update(salt)
                .digest('hex')
}


module.exports.getServerIP = getServerIP
module.exports.getTotalSales = getTotalSales
module.exports.getTotalPOSDetailSales = getTotalPOSDetailSales
module.exports.calculateTopSellerPOSDetails = calculateTopSellerPOSDetails
module.exports.generateRandomId = generateRandomId
module.exports.calculateTotalSaleOrders = calculateTotalSaleOrders
module.exports.calculateTotalPurchases = calculateTotalPurchases
module.exports.isValidDate = isValidDate
module.exports.getTotalExpenses = getTotalExpenses
module.exports.calculateTotalDeposit = calculateTotalDeposit
module.exports.generatePasswordSalt = generatePasswordSalt
module.exports.encryptUserPassword = encryptUserPassword