require('dotenv').config()

module.exports = { 
    APP_PORT: process.env.APP_PORT,
    SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
     //used when we create and verify JSON Web Tokens
    TOKEN_SECRET: process.env.TOKEN_SECRET,
}