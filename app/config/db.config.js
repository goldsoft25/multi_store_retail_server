
require('dotenv').config()

module.exports = {
  HOST: process.env.DB_HOST,
  USER: process.env.DB_USER,
  PASSWORD: process.env.DB_PASS,
  DB: process.env.DB_NAME,
  dialect: "mysql", //ORM to change for different MYSQL databases.
  pool: { // it will be used for Sequelize connection pool configuration:
    max: 300, //maximum number of connection in pool
    min: 0,
    acquire: 30000, //maximum time, in milliseconds, that pool will try to get connection before throwing error 
    idle: 10000 ////maximum time, in milliseconds, that a connection can be idle before being released
  },
  //logging: process.env.DB_LOGGING
  logging:true
}