/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

 const {generateRandomId, generatePasswordSalt, encryptUserPassword} = require('../utils/common')

 module.exports = (sequelize, Sequelize)=>{
    const Employee = sequelize.define('employee', {
        employeeId:{
           type: Sequelize.STRING,
           primaryKey: true,
           defaultValue: generateRandomId()
        }, 
        username:{
            type: Sequelize.STRING,
            allowNull:false,
            unique:true
        },
        password:{
            type: Sequelize.STRING,
            allowNull:false,
            get(){ //to tell Sequelize to treat those rows as functions instead 
                //of variables, preventing them from showing up on queries like findAll() or findById(1)
                return () => this.getDataValue('password')
            }
        },
        salt:{
           type: Sequelize.STRING,
           allowNull: false,
           get(){
              return () => this.getDataValue('salt')
           }
        },
        status:{
            type: Sequelize.STRING,
            allowNull:false
        },
        role:{
            type: Sequelize.STRING,
            allowNull: false
        },
        business_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}

    })

    //Salt is a unique encryption key used to dynamically encrypt the users password.
    //We use a unique key so that later we can check password validity by encrypting their password entry and seeing if it matches the original encrypted password in the database.
    /*const setSaltAndPassword = user=>{
        console.log('employee detail', user)
        if (user.changed('password')) {
            //user.salt = Employee.generatePasswordSalt()
            user.salt = generatePasswordSalt()
            user.password = Employee.encryptUserPassword(user.password(), user.salt()) 
        }
    }*/

     
    //Sequelize hooks - listen to sequelize events
    //Every time a user row is created or a password is updated, a new salt 
    //will be generated and the password will be automatically encrypted.
    //Employee.beforeCreate(setSaltAndPassword)
    //Employee.beforeUpdate(setSaltAndPassword)
    /*Employee.beforeCreate((user, options)=>{
        return new Promise((resolve,reject)=>{
            const salt = generatePasswordSalt()
            user.password = encryptUserPassword(user.password, salt)
            console.log('employee', user)
            resolve(user)
        })
    })

    //But how do we check and see if they have entered the correct password?
    Employee.prototype.checkCorrectPassword = function(enteredPassword){
        return Employee.encryptUserPassword(enteredPassword, 
            this.salt() === this.password
        )
    }*/
    // sequelize hooks are disturbing, skip them for now

    return Employee
 }