/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

module.exports = (sequelize, Sequelize) => {
    const Product = sequelize.define("product", {
        prod_name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique:true,
        },
        prod_code:{
            type: Sequelize.STRING,
        },
        prod_description:{
            type: Sequelize.TEXT
        },
        unit_price:{
            type: Sequelize.DECIMAL,
            validate:{  isNumeric: true },
            allowNull: false
        },
        retail_price:{ 
            type: Sequelize.DECIMAL,
            validate:{  isNumeric: true },
            allowNull:false
        },
        orig_stock:{ 
            type: Sequelize.INTEGER,
            validate:{  isNumeric: true },
            allowNull: false 
        },
        reorder_level: { 
            type: Sequelize.INTEGER ,
            validate:{  isNumeric: true }
        },
        prod_category: { type: Sequelize.STRING, allowNull: false},
        expiry_date: { 
            type: Sequelize.BOOLEAN 
        },
        batchEnabled: {
            type: Sequelize.BOOLEAN
        },
        qty_sold: { 
            type: Sequelize.INTEGER,
            validate: { isNumeric: true } 
        },
        brand_name: { type: Sequelize.STRING },
        standard_measure_unit:{ type: Sequelize.STRING },
        piece_measure_unit: {type: Sequelize.STRING},
        piece_unit_price: {
            type: Sequelize.DECIMAL,
            validate:{  isNumeric: true }
        },
        standard_piece_ratio_form1:{
            type: Sequelize.FLOAT,
            validate: { isNumeric: true }  
        },
        standard_piece_ratio_form2:{
            type: Sequelize.FLOAT,
            validate: { isNumeric: true }  
        },
        discount_rate:{
            type: Sequelize.FLOAT,
            validate: { isNumeric: true}
        },
        shopId: { type: Sequelize.STRING, allowNull: false},
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}

    })
    return Product
}