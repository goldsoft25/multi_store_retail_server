/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

 const { generateRandomId } = require('../utils/common')

 module.exports = (sequelize, Sequelize)=>{
     const sale = sequelize.define('sales', {
        prod_name:{
            type: Sequelize.STRING,
            allowNull:false
        },
        retail_price: {
            type: Sequelize.DECIMAL, //Decimal is best for money
            validate: { isNumeric: true},
            allowNull: false
        },
        costPrice: {
            type: Sequelize.DECIMAL, //Decimal is best for money
            validate: { isNumeric: true},
            allowNull: false
        },
        sales_qty:{
            type: Sequelize.INTEGER,
            validate:{  isNumeric: true },
            allowNull: false 
        },
        who_sold:{
            type: Sequelize.STRING,
            allowNull:false
        },
        batchCode: {
            type:  Sequelize.STRING
        },
        paymentOption:{
            type: Sequelize.STRING, allowNull: false,  
        },
        depositAmount:{
            type: Sequelize.DECIMAL,
            validate:{  isNumeric: true }
        },
        customerId: {
            type: Sequelize.STRING,
            allowNull: false
        },
        soldAt: {
            type: Sequelize.DATE,
            allowNull: false
        },
        referenceId:{  //for creditPayments
            type: Sequelize.STRING,
            defaultValue: generateRandomId(),
            allowNull:false
        },
        shopId: { type: Sequelize.STRING, allowNull: false},
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}
     })
     return sale
  }