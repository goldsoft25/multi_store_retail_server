/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

 module.exports = (sequelize, Sequelize)=>{
    const expense = sequelize.define('expense-category', {
        cat_name:{
            type: Sequelize.STRING,
            allowNull:false
        },
        cat_shopId:{
            type: Sequelize.STRING,
            allowNull:false
        },
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}
    })
    return expense
}