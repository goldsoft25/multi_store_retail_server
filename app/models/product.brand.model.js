/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

 module.exports = (sequelize, Sequelize) => {
    const brand = sequelize.define('product-brand',{
        brand_name:{
           type: Sequelize.STRING,
           allowNull:false,
           unique: true
        },
        brand_shopId:{
           type: Sequelize.STRING,
           allowNull:false
        },
        synced: {
           type: Sequelize.BOOLEAN,
           allowNull:false
        },
        closed: {
           type: Sequelize.BOOLEAN,
           allowNull:false
        }
    })
    return brand
 }