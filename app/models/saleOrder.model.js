/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

const { generateRandomId } = require('../utils/common')

 module.exports = (sequelize, Sequelize)=>{
    const saleOrder = sequelize.define('sale-order', { 
        
        referenceId:{
            type: Sequelize.STRING,
            primaryKey: true,
            defaultValue: generateRandomId()
        },
        products:{
            //type: Sequelize.ARRAY, --> only supported in  Postgres
            type: Sequelize.STRING,
            allowNull:false
        },
        totalAmount: {
            type: Sequelize.DECIMAL, //Decimal is best for money
            validate: { isNumeric: true},
            allowNull: false
        },
        client: {
            type: Sequelize.STRING,
            allowNull: false
        },
        order_date:{
           type: Sequelize.DATE,
           allowNull: false
        },
        depositAmount:{
            type: Sequelize.DECIMAL,
            validate: { isNumeric: false},
            allowNull:false
        },
        shipped_date:{
            type: Sequelize.DATE
        },
        paymentOption:{ 
            type: Sequelize.STRING, 
            allowNull:false
        },
        status:{
            type: Sequelize.STRING,
            allowNull: false
        },
        actor:{ type: Sequelize.STRING, allowNull: false},
        shopId: { type: Sequelize.STRING, allowNull: false},
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}
    })

    return saleOrder
 }