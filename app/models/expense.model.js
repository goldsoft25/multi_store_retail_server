/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

 module.exports = (sequelize, Sequelize)=>{
    const expense = sequelize.define('expense', {
        exp_category:{
            type: Sequelize.STRING,
            allowNull:false
        },
        expense_description:{
            type: Sequelize.STRING,
            allowNull:false
        },
        exp_amount: {
            type: Sequelize.DECIMAL,
            allowNull:false
        },
        exp_status:{
            type: Sequelize.STRING,
            allowNull:false
        },
        exp_handler: {
            type: Sequelize.STRING,
            allowNull:false
        },
        startDate: {
            type: Sequelize.DATE,
            allowNull:false
        },
        endDate: {
            type: Sequelize.DATE,
            allowNull:false
        },
        merchant:{type: Sequelize.STRING},
        exp_rate: {
            type: Sequelize.INTEGER,
            allowNull: false,
            validate: { isNumeric : true}
        },
        approved_on:{
           type: Sequelize.DATE
        },
        business_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}
    })
    return expense
}