/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

const {generateRandomId} = require('../utils/common')

 module.exports = (sequelize, Sequelize)=>{
    const contact = sequelize.define('contact', {
        contactId:{
           type: Sequelize.STRING,
           primaryKey: true,
           defaultValue: generateRandomId()
        }, 
        firstName:{
            type: Sequelize.STRING,
            allowNull:false
        },
        lastName:{
            type: Sequelize.STRING,
            allowNull:false
        },
        primary_contact:{
            type: Sequelize.STRING,
            allowNull:false,
            unique:true
        },
        other_contact:{
            type: Sequelize.STRING
        },
        company_name:{
            type: Sequelize.STRING
        },
        company_street:{
            type: Sequelize.STRING
        },
        email:{
            type: Sequelize.STRING
        },
        contact_type:{
            type: Sequelize.STRING,
            allowNull: false
        },
        notes:{ type: Sequelize.TEXT},
        business_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}

    })
     return contact 
 }