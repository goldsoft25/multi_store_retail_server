/**
 * https://sequelize.org/master/manual/model-basics.html
 * Model represents the abstraction of the table in database.
 * The model tells Sequelize several things about the entity it represents,
 *  such as the name of the table in the database and which columns it has (and their data types).
 * Models can be defined in two equivalent ways in Sequelize:
 * - Calling sequelize.define(modelName, attributes, options)
 *   //Internally, sequelize.define calls Model.init, so both approaches are essentially equivalent.
 * - Extending Model and calling init(attributes, options)
 * @param {*} sequelize 
 * @param {*} Sequelize 
 */

 const { generateRandomId } = require('../utils/common')

 module.exports = (sequelize, Sequelize)=>{
    const creditPayment = sequelize.define('credit-payment', {
        ////get it from referenceId of sales + new-stock
        referenceId:{ 
            type: Sequelize.STRING,
            allowNull:false
        },
        category:{
            type: Sequelize.STRING,
            allowNull:false
        },
        contactId: {
            type: Sequelize.STRING,
            allowNull:false
        },
        depositAmount:{
            type: Sequelize.DECIMAL,
            allowNull:false,
            validate:{ isNumeric: true}
        },
        originalDebt: {
            type: Sequelize.DECIMAL,
            allowNull:false,
            validate:{ isNumeric: true}
        },
        balance: {
            type: Sequelize.DECIMAL,
            allowNull:false,
            validate:{ isNumeric: true}
        },
        note: {
            type: Sequelize.STRING,
            allowNull:false
        },
        soldAt:{
            type: Sequelize.DATE,
            allowNull:false
        },
        shopId: {
            type: Sequelize.STRING,
            allowNull: false
        },
        synced: {type: Sequelize.BOOLEAN},
        closed: {type: Sequelize.BOOLEAN}
    })
    return creditPayment
}