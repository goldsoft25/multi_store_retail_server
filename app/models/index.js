const dbConfig = require("../config/db.config.js");


const Sequelize = require("sequelize");
////initialise the sequelize  database connection
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false,
  logging: dbConfig.logging,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

//test if the connection is okay.
try{
  sequelize.authenticate()
  console.log('Connection has been established successful')
}catch(error){
   console.log('Failed to establish connection', error)
}

const db = {}
db.Sequelize = Sequelize // always refers to the library itself
db.sequelize = sequelize // refers to instance of Sequelize that represents the connection to one database


//bizwatch retail.
db.products = require("./product.model")(sequelize, Sequelize);
db.productCategories = require("./product.category.model")(sequelize, Sequelize)
db.productBrand = require('./product.brand.model')(sequelize, Sequelize)
db.stocklist = require('./stocklist.model')(sequelize, Sequelize)
db.sales = require('./sale.model')(sequelize,Sequelize)
db.posDetail = require('./posDetail.model')(sequelize, Sequelize)
db.saleOrder = require('./saleOrder.model')(sequelize, Sequelize)
db.purchaseOrder = require('./purchaseOrder.model')(sequelize, Sequelize)
db.Contact = require('./contact.model')(sequelize, Sequelize)
db.Expense = require('./expense.model')(sequelize, Sequelize)
db.ExpenseCategory = require('./expense.category.model')(sequelize, Sequelize)
db.CreditPayment = require('./credit.payment.model')(sequelize, Sequelize)
db.Employee = require('./employee.model')(sequelize, Sequelize)

//define one to many relationships (focus on foreign key implementation)
// one Product can have  many stocklist, but one stocklist only belongs to one Product.
db.products.hasMany(db.stocklist, {
   as: 'stocklist'
}) 
db.stocklist.belongsTo(db.products, {
  foreignKey: 'productId',
  as: 'product'
})


module.exports = db