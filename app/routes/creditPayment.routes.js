/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/creditPayment: GET, POST, DELETE
 /api/creditPayment/:id: GET, PUT, DELETE
 /api/creditPayment/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const creditPayment = require('../controllers/creditPayment.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newCreditValidator = [
        check('referenceId').isString().isLength({min:5}),
        check('category').isString().isLength({min:3}),
        check('contactId').isString().isLength({min:5}),
        check('shopId').isString().isLength({min:5})
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })


    router.get('/', creditPayment.findAllCreditPayment)

    router.post('/', newCreditValidator,creditPayment.multi_store_addCreditPayment)


    //mount the routes to express application.
    app.use('/api/creditPayment', router)

}