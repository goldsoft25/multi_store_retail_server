/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/sales: GET, POST, DELETE
 /api/sales/:id: GET, PUT, DELETE
 /api/sales/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const sales = require('../controllers/sale.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newSaleValidator = [
        check('prod_name').isString().isLength({min:3}),
        check('retail_price').isNumeric(),
        check('costPrice').isNumeric(),
        check('sales_qty').isNumeric(),
        check('who_sold').isString().isLength({min:3}),
        check('customerId').isString().isLength({min:5}),
        check('soldAt').isString(), //JSON does not allow date objects
        check('paymentOption').isString()
    ]

    const editSaleValidator = [
        check('updateItem').isObject(),
        check('productStock').isObject()
    ]

    const metricValidator = [
        check('startDate').isString().isLength({min:4}),
        check('endDate').isString().isLength({min:4}),
        check('shopId').isString().isLength({min:5})
    ]


    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

     //create newSale
    router.post("/", newSaleValidator, sales.createNewSale)

     //Retrieve all  Sales
    router.get("/", sales.findAllSales);

     //Update the salesItem.
    router.put("/:id", editSaleValidator, sales.updateSaleItem)

    //Delete the salesItem.
    router.delete("/:id", sales.deleteSaleItem)
    
    //TopSellingItems metrics
    router.post('/topSellingItems', metricValidator, sales.pickTopSellingItems )

    //sale credit metrics.
    router.post('/creditMetrics', metricValidator ,sales.salesCreditDebt)
    
    //mount the routes to express application.
    app.use('/api/sales', router)

}