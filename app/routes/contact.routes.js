/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/contact: GET, POST, DELETE
 /api/contact/:id: GET, PUT, DELETE
 /api/contact/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const contact = require('../controllers/contact.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newContactValidator = [
        check('firstName').isString().isLength({min:3}),
        check('lastName').isString().isLength({min:3}),
        check('primary_contact').isString().isLength({min:3}),
        check('contact_type').isString().isLength({min:4}),
        check('business_id').isString().isLength({min:5})
    ]

    const editContactValidator = [
        check('updatedFields.primary_contact').isString().isLength({min:3}),
        check('updatedFields.other_contact').isString(),
        check('updatedFields.company_name').isString(),
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

     //create new Contact
    router.post("/", newContactValidator, contact.createNewContact)

     //Retrieve all  contacts
    router.get("/", contact.findAllContact);

     //Update the contactItem.
    router.put("/:id", editContactValidator, contact.updateContact )

    //Delete the contactItem.
    router.delete("/:id", contact.deleteContactItem)
    
    //mount the routes to express application.
    app.use('/api/contact', router)

}