/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/purchaseOrder: GET, POST, DELETE
 /api/purchaseOrder/:id: GET, PUT, DELETE
 /api/purchaseOrder/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const purchaseOrder = require('../controllers/purchaseOrder.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newPurchaseOrderValidator = [
        check('paymentOption').isString().isLength({min:2}),
        check('totalAmount').isFloat(),
        check('client').isString().isLength({min:5}),
        check('shopId').isString().isLength({min:5}),
        check('actor').isString().isLength({min:3}),
        check('depositAmount').isFloat(),
    ]

    const editPurchaseOrderValidator = [
        check('updatedFields').isObject(),
        check('productUpdates').isArray(),
        check('removeProducts').isArray()
    ]

    const orderMetricValidator = [
        check('startDate').isString().isLength({min:4}),
        check('endDate').isString().isLength({min:4}),
        check('shopId').isString().isLength({min:5})
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

    router.post('/', newPurchaseOrderValidator, purchaseOrder.createPurchaseOrder)
     
    //fetch all the purchaseOrders
    router.get('/', purchaseOrder.findAllPurchaseOrders )
    
    //update the purchaseOrder
    router.put('/:id', editPurchaseOrderValidator,purchaseOrder.updatePurchaseOrderItem)

    //delete the purchaseOrder
    router.delete('/:id', purchaseOrder.deletePurchaseOrderItem)

    //purchaseOrder-metrics
    router.post('/purchaseOrderMetrics',orderMetricValidator, purchaseOrder.pickTotalPurchaseOrder)


    //mount the routes to express application.
    app.use('/api/purchaseOrder', router)
}