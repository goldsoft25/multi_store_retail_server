/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/saleOrder: GET, POST, DELETE
 /api/saleOrder/:id: GET, PUT, DELETE
 /api/saleOrder/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const saleOrder = require('../controllers/saleOrder.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newSaleOrderValidator = [
        check('paymentOption').isString().isLength({min:2}),
        check('totalAmount').isNumeric(),
        check('client').isString().isLength({min:5}),
        check('shopId').isString().isLength({min:5}),
        check('actor').isString().isLength({min:3}),
        check('depositAmount').isNumeric(),
    ]

    const editSaleOrderValidator = [
        check('updatedFields').isObject(),
        check('productUpdates').isArray(),
        check('removeProducts').isArray()
    ]

    const orderMetricValidator = [
        check('startDate').isString().isLength({min:4}),
        check('endDate').isString().isLength({min:4}),
        check('shopId').isString().isLength({min:5})
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

    router.post('/', newSaleOrderValidator, saleOrder.createSaleOrder)
     
    //fetch all the saleOrders
    router.get('/', saleOrder.findAllSaleOrders )
    
    //update the saleOrder
    router.put('/:id', editSaleOrderValidator,saleOrder.updateSaleOrderItem)

    //delete the saleOrder
    router.delete('/:id', saleOrder.deleteSaleOrderItem)

    //saleOrder-metrics
    router.post('/saleOrderMetrics',orderMetricValidator, saleOrder.pickTotalSaleOrder)
    
    //credit metrics.
    router.post('/creditMetrics', orderMetricValidator, saleOrder.saleOrderCreditDebt )

    //mount the routes to express application.
    app.use('/api/saleOrder', router)
}