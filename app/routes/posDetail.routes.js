/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/posDetail: GET, POST, DELETE
 /api/posDetail/:id: GET, PUT, DELETE
 /api/posDetail/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const posDetail = require('../controllers/posDetail.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newPOSValidator = [
        check('receiptNo').isString().isLength({min:5}),
        check('cashReceived').isNumeric(),
        check('totalAmount').isNumeric(),
        check('customerId').isString().isLength({min:5}),
        //check('receiptItems').isString().isLength({min:1})
    ]

    const posMetricValidator = [
        check('startDate').isString().isLength({min:4}),
        check('endDate').isString().isLength({min:4}),
        check('shopId').isString().isLength({min:5})
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

    router.post('/', newPOSValidator, posDetail.createNewPOSDetail)
     
    //loadReceiptHistory
    router.get('/', posDetail.findAllPOSDetail )

    //posDetail-metrics
    router.post('/metrics',posMetricValidator, posDetail.filteredPOSItemsByDate)


    //mount the routes to express application.
    app.use('/api/posDetail', router)
}