/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/contact: GET, POST, DELETE
 /api/contact/:id: GET, PUT, DELETE
 /api/contact/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const business = require('../controllers/business.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newEmployeeValidator = [
        check('emp_username').isString().isLength({min:3}),
        check('emp_password').isString().isLength({min:4}),
        check('emp_role').isString().isLength({min:3}),
        check('biz_code').isString().isLength({min:4})
    ]

    const editContactValidator = [
        check('updatedFields.primary_contact').isString().isLength({min:3}),
        check('updatedFields.other_contact').isString(),
        check('updatedFields.company_name').isString(),
    ]

    const loginDetailValidator = [
        check('username').isString().isLength({min:3}),
        check('password').isString().isLength({min:3})
    ]

     //create new Employee
    router.post("/", newEmployeeValidator, business.createNewEmployee)

     //Retrieve all employees
    router.get("/", business.findAllEmployees);

     //Update the contactItem.
    //router.put("/:id", editContactValidator, contact.updateContact )

    //Delete the contactItem.
    //router.delete("/:id", contact.deleteContactItem)

    //login the employee.
    router.post('/login', loginDetailValidator ,business.loginAsEmployee)
    
    //mount the routes to express application.
    app.use('/api/employees', router)

}