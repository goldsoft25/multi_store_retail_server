/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/stocklist: GET, POST, DELETE
 /api/stocklist/:id: GET, PUT, DELETE
 /api/stocklist/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const stocklist = require('../controllers/stocklist.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newStockValidator = [
        check('productId').isLength({min:1}),
        check('costPrice').isNumeric(),
        check('sales_qty').isNumeric(),
        check('who_sold').isString().isLength({min:3}),
        //check('arrivalDate').isDate(), //json does not support date objects.
        check('arrivalDate').isString(),
        check('paymentOption').isString()
    ]

    const editStockValidator = [
        check('productId').isLength({min:1}),
        check('productStock').isNumeric(),
        check('updateItem').isObject()
    ]

    const totalPurchaseValidator = [
        check('startDate').isString().isLength({min:4}),
        check('endDate').isString().isLength({min:4}),
        check('shopId').isString().isLength({min:5})
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

     //create newStock
    router.post('/', newStockValidator, stocklist.createNewStock)

     //Retrieve all  incomingStock
    router.get("/", stocklist.findAllStocklist);

     //Update the stocklistItem.
    router.put("/:id", editStockValidator, stocklist.updateStocklistItem)

    //Delete the stocklistItem.
    router.delete("/:id", stocklist.deleteStocklistItem)

    //Total purchases for stsocklist.
    router.post('/totalPurchases',totalPurchaseValidator,stocklist.calculateTotalPurchases)

    //mount the routes to express application.
    app.use('/api/stocklist', router)

}
