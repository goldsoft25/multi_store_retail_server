/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/expense: GET, POST, DELETE
 /api/expense/:id: GET, PUT, DELETE
 /api/expense/published: GET
*/

//what about the input validators.
module.exports = app =>{
    const expense = require('../controllers/expense.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    const newExpenseValidator = [
        check('exp_category').isString().isLength({min:3}),
        check('expense_description').isString().isLength({min:3}),
        check('exp_amount').isFloat(),
        check('exp_status').isString().isLength({min:4}),
        check('exp_handler').isString().isLength({min:4}),
        check('startDate').isString(),
        check('endDate').isString(),
        check('business_id').isString().isLength({min:5})
    ]

    const editExpenseValidator = [
        check('updatedFields.expense_description').isString().isLength({min:3}),
        check('updatedFields.exp_amount').isFloat(),
        check('updatedFields.merchant').isString(),
        check('updatedFields.exp_status').isString(),
        check('updatedFields.startDate').isString(),
        check('updatedFields.endDate').isString()
    ]

    const newCategoryValidator = [
        check('cat_name').isString().isLength({min:3}),
        check('cat_shopId').isString().isLength({min:4})
    ]

    const expenseMetricValidator = [
        check('startDate').isString().isLength({min:4}),
        check('endDate').isString().isLength({min:4}),
        check('shopId').isString().isLength({min:5})
    ]

    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

     //create new Expense
    router.post("/", newExpenseValidator, expense.createNewExpense)

     //Retrieve all  expenses
    router.get("/", expense.findAllExpenses);

     //Update the expenseItem.
    router.put("/:id", editExpenseValidator, expense.updateExpense )

    //Delete the expenseItem.
    router.delete("/:id", expense.deleteExpenseItem)

    //Expense categories.
    router.get('/category', expense.findAllExpenseCategory)

    router.post('/category', newCategoryValidator ,expense.createExpenseCategory)

    router.put('/category/:id', expense.updateExpenseCategory )

    router.delete('/category/:id',  expense.deleteExpenseCategory)

    //Expense metrics.
    router.post('/totalExpenses', expenseMetricValidator ,expense.pickTotalExpensesByCategory)
    
    //mount the routes to express application.
    app.use('/api/expense', router)

}