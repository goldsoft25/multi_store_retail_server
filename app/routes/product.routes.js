/* Define the routes
 * When a client sends request for an endpoint using HTTP request (GET, POST, PUT, DELETE),
 * These are our routes:
 /api/products: GET, POST, DELETE
 /api/products/:id: GET, PUT, DELETE
 /api/products/published: GET
*/

//what about the input validators.


module.exports = app => {
    const products = require('../controllers/product.controller')
    
    const router = require('express').Router()

    const {check, validationResult } = require('express-validator')

    //express validators.
    const newProductValidator = [
        check('prod_name').isString().isLength({min:3}),
        check('unit_price').isNumeric(),
        check('retail_price').isNumeric(),
        check('orig_stock').isNumeric(),
        check('reorder_level').isNumeric(),
        check('prod_category').isString().isLength({min:3}),
        check('shopId').isString().isLength({min:5})
    ]

    const editProductValidator = [
        check('prod_description').isString(),
        check('unit_price').isNumeric(),
        check('retail_price').isNumeric(),
        check('reorder_level').isNumeric(),
        check('prod_category').isString().isLength({min:3})
    ]

    const newProductCategoryValidator = [
        check('cat_name').isString().isLength({min:2}),
        check('cat_shopId').isString().isLength({min:5})
    ]

    const newBrandItemValidator = [
       check('brand_name').isString().isLength({min:3}),
       check('brand_shopId').isString().isLength({min:5})
    ]

    const editBrandItemValidator = [
        check('brand_name').isString().isLength({mmin:3})
    ]


    const { isUserAuthenticated } = require('../utils/jwtTokenMiddleware')

    //Authentication middleware.
    router.use(isUserAuthenticated, (req,res,next)=>{
       next()
    })

    //create new product.
    router.post('/', newProductValidator, products.createProduct)

    //Retrieve all products
    router.get("/", products.findAllProducts);

    // Retrieve all published products
    //router.get("/published", products.findAllPublished);

    // Retrieve a single Product with id
    //router.get("/:id", products.findOne);

    // Update a Product with id
    router.put("/:id", editProductValidator, products.updateProduct);

    // Delete a Product with id
    router.delete("/:id", products.deleteProduct);

    // Delete all products
    //router.delete("/", products.deleteAll);

    //PRODUCT CATEGORY
    //create the product category
    router.post('/category', newProductCategoryValidator, products.createProductCategory);
    
    //Retrieve all the product categories
    router.get('/category', products.findAllProductCategory)

    router.put('/category/:id', 
        [check('cat_name').isString().isLength({min:3})],
        products.updateProductCategory
    )

    router.delete('/category/:id', products.deleteProductCategory)

    //PRODUCT BRAND
    router.post('/brandItem', newBrandItemValidator, products.createProductBrand )

    router.get('/brandItem', products.findAllProductBrand)

    router.put('/brandItem/:id', editBrandItemValidator, products.updateProductBrand)

    router.delete('/brandItem/:id', products.deleteProductBrand)

    //check multi-store server health.
    router.get('/pingServer', )

    //mount the routes to express application.
    app.use('/api/products', router)

}